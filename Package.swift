// swift-tools-version:5.2

import PackageDescription

let package = Package(
    name: "SwiftEx",
    platforms: [
        .macOS(.v10_15)
//        .iOS(.v12)
    ],
    products: [
        .library(name: "SwiftEx", targets: ["SwiftEx"]),
        .library(name: "SwiftExData", targets: ["SwiftExData"]),
        .library(name: "SwiftExUI", targets: ["SwiftExUI"]),
        .library(name: "SwiftExApi", targets: ["SwiftExApi"]),
        .library(name: "SwiftExVapor", targets: ["SwiftExVapor"])
    ],
    dependencies: [
//        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "5.0.0")
        .package(url: "https://github.com/seriyvolk83/SwiftyJSON", from: "5.0.2"),
        .package(url: "https://github.com/vapor/vapor.git", from: "4.0.0")
    ],
    targets: [
        .target(name: "SwiftEx", path: "Source"),
        .target(name: "SwiftExData", dependencies: ["SwiftEx", "SwiftyJSON"], path: "SourceData"),
        .target(name: "SwiftExUI", dependencies: ["SwiftEx", "SwiftExData"], path: "SourceUI"),
        .target(name: "SwiftExApi", dependencies: ["SwiftEx", "SwiftyJSON", "SwiftExData"], path: "SourceApi"),
        .target(name: "SwiftExVapor", dependencies: ["SwiftEx", "SwiftyJSON", "SwiftExData", "SwiftExApi", .product(name: "Vapor", package: "vapor")], path: "SourceVapor")
])

