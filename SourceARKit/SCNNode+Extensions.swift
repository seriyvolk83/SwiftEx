//
//  SCNNode+Extensions.swift
//  SwiftEx83/ARKit
//
//  Created by Volkov Alexander on 09/29/2020.
//  Copyright (c) 2020 Alexander Volkov. All rights reserved.
//

import ARKit

extension SCNNode {
    
    /// Create line
    /// - Parameters:
    ///   - p1: the first point
    ///   - p2: the second point
    ///   - lineWidth: the line width
    ///   - lineHeight: (optional) line height
    ///   - material: the material to apply
    ///   - geometry: (optional) the geometry to use
    /// - Returns: the created node
    @available(iOS 11.0, *)
    public static func createLine(p1: SCNVector3, p2: SCNVector3, lineWidth: Float, lineHeight: Float? = nil, material: SCNMaterial, geometry: SCNGeometry = SCNBox(width: 1, height: 1, length: 1, chamferRadius: 0)) -> SCNNode {
        let lineHeight = lineHeight ?? lineWidth
        geometry.materials = [material]
        let lineNode = SCNNode(geometry: geometry)
        
        let distance = p1.distance(to: p2)
        let scale = makeScaleMatrix(xScale: distance, yScale: lineHeight, zScale: lineWidth)
        let angle = p1.angleOnXZ(to: p2)
        let angleXZProj = -1 * p1.angleWithXZProjection(to: p2)
        let rotationY = makeRotationYMatrix(angle: Float(angle))
        let rotationZ = makeRotationZMatrix(angle: Float(angleXZProj))
        let translation = SCNVector3().translation(to: p1.center(between: p2))
        
        lineNode.simdTransform = translation * rotationY * rotationZ * scale
        return lineNode
    }
    
    /// Create circle point. Can be used for debugging
    /// - Parameters:
    ///   - position: the position
    ///   - radius: the radius (20cm by default)
    ///   - material: the material (color material is used by default)
    ///   - colorForDefaultMaterial: the color of the default material (red by defailt)
    public func createPoint(_ position: SCNVector3, radius: CGFloat = 0.2, material: SCNMaterial? = nil, colorForDefaultMaterial: UIColor = .red) -> SCNNode {
        var material: SCNMaterial! = material
        if material == nil {
            let m = SCNMaterial(); m.isDoubleSided = true; m.diffuse.contents = colorForDefaultMaterial.withAlphaComponent(0.8)
            material = m
        }
        
        let shape = SCNSphere(radius: radius)
        shape.materials = [material!]
        let node = SCNNode(geometry: shape)
        node.position = position
        return node
    }
}
