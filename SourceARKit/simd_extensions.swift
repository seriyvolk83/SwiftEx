//
//  simd_extensions.swift
//  SwiftEx83/ARKit
//
//  Created by Volkov Alexander on 09/29/2020.
//  Copyright (c) 2020 Alexander Volkov. All rights reserved.
//

import ARKit

/// Make scale matrix
public func makeScaleMatrix(xScale: Float, yScale: Float, zScale: Float) -> simd_float4x4 {
    let rows = [
        simd_float4(xScale,      0,      0, 0),
        simd_float4(     0, yScale,      0, 0),
        simd_float4(     0,      0, zScale, 0),
        simd_float4(     0,      0,      0, 1)
    ]
    return float4x4(rows: rows)
}

/// Make translation matrix
public func makeTranslationMatrix(tx: Float, ty: Float, tz: Float) -> simd_float4x4 {
    var matrix = matrix_identity_float4x4
    
    matrix[3, 0] = tx
    matrix[3, 1] = ty
    matrix[3, 2] = tz
    
    return matrix
}

/// Make rotation matrix
/// - Parameter angle: the angle on plane YZ
public func makeRotationXMatrix(angle: Float) -> simd_float4x4 {
    let rows = [        
        simd_float4( 1,          0,           0, 0),
        simd_float4( 0, cos(angle),  sin(angle), 0),
        simd_float4( 0, -sin(angle), cos(angle), 0),
        simd_float4( 0,          0,           0, 1)
    ]
    
    return float4x4(rows: rows)
}

/// Make rotation matrix
/// - Parameter angle: the angle on plane XZ
public func makeRotationYMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), 0, sin(angle), 0),
        simd_float4( 0,          1,          0, 0),
        simd_float4(-sin(angle), 0, cos(angle), 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

/// Make rotation matrix
/// - Parameter angle: the angle on plane XY
public func makeRotationZMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), sin(angle), 0, 0),
        simd_float4(-sin(angle), cos(angle), 0, 0),
        simd_float4( 0,          0,          1, 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

extension simd_float3 {
    
    func distance() -> Float {
        return simd_distance(self, simd_float3.init())
    }
}

extension simd_float4 {
    
    public var vector: SCNVector3 {
        return SCNVector3(x, y, z)
    }
    
    /// Convert to simd_float3
    public func toSimd3() -> simd_float3 {
        return simd_float3(x: x, y: y, z: z)
    }
}

extension simd_float4x4 {
    
    // Scale all axis y `scale`
    public func scale(by scale: Float) -> float4x4 {
        let rows = [
            simd_float4(scale, 0, 0, 0),
            simd_float4(0, scale, 0, 0),
            simd_float4(0, 0, scale, 0),
            simd_float4(0, 0, 0, 1),
        ]
        return float4x4(rows: rows) * self
    }

    /// Calculate position vector of a pose matrix
    /// - Returns: A SCNVector3 from the translation components of the matrix
    public func position() -> SCNVector3 {
        return SCNVector3(columns.3.x, columns.3.y, columns.3.z)
    }
}
