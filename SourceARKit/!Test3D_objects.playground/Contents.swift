import SceneKit
import QuartzCore   // for the basic animation
import XCPlayground // for the live preview
import PlaygroundSupport
typealias Color = UIColor

/**
 This code provides different samples on how to work with 3D in Swift.
 1. Scroll to the end to "Testing" section and uncomment one of the `test*()` methods call.
 2. Run the playground and check the method doing.
 3. Check its code on how it's implemented.
 4. Check other `test*()` methods (not presented in "Testing" section).
 5. Uncomment/comment some lines in `test*()` method - it may provide a few different results.
 */

// create a scene view with an empty scene
var sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: 600, height: 600))
var scene = SCNScene()
sceneView.scene = scene

// start a live preview of that view
PlaygroundPage.current.liveView = sceneView

// default lighting
sceneView.autoenablesDefaultLighting = true

// a camera
var cameraNode = SCNNode()
cameraNode.camera = SCNCamera()
cameraNode.position = SCNVector3(x: 5, y: 10, z: 10)
cameraNode.look(at: SCNVector3())
scene.rootNode.addChildNode(cameraNode)

// a geometry object
var torus = SCNTorus(ringRadius: 1, pipeRadius: 0.35)
var torusNode = SCNNode(geometry: torus)
//scene.rootNode.addChildNode(torusNode)

// configure the geometry object
torus.firstMaterial?.diffuse.contents  = Color.yellow   // (or UIColor on iOS)
torus.firstMaterial?.specular.contents = Color.white // (or UIColor on iOS)

// set a rotation axis (no angle) to be able to
// use a nicer keypath below and avoid needing
// to wrap it in an NSValue
torusNode.rotation = SCNVector4(x: 1.0, y: 1.0, z: 0.0, w: 0.0)

// animate the rotation of the torus
var spin = CABasicAnimation(keyPath: "rotation.w") // only animate the angle
spin.toValue = 2.0*Double.pi
spin.duration = 3
spin.repeatCount = HUGE // for infinity
//torusNode.addAnimation(spin, forKey: "spin around")

var moveCamera = CABasicAnimation(keyPath: "translation.x") // only animate the angle
moveCamera.toValue = 10
moveCamera.duration = 3
moveCamera.autoreverses = true
moveCamera.repeatCount = HUGE // for infinity
cameraNode.addAnimation(moveCamera, forKey: "translation.x")

//var moveCameraZ = CABasicAnimation(keyPath: "translation.z") // only animate the angle
//moveCameraZ.toValue = 10
//moveCameraZ.duration = 3
//moveCameraZ.autoreverses = true
//moveCameraZ.repeatCount = HUGE // for infinity

//let g = CAAnimationGroup()
//g.animations = [moveCamera]
//g.duration = 3
//g.repeatCount = HUGE
//cameraNode.addAnimation(g, forKey: "translation.z")

func makeTranslationMatrix(tx: Float, ty: Float, tz: Float) -> simd_float4x4 {
    var matrix = matrix_identity_float4x4
    
//    matrix[0, 3] = tx
//    matrix[1, 3] = ty
//    matrix[2, 3] = tz
    matrix[3, 0] = tx
    matrix[3, 1] = ty
    matrix[3, 2] = tz
    
    return matrix
}

func makeRotationXMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4(1,          0,          0, 0),
        simd_float4(0, cos(angle), sin(angle), 0),
        simd_float4(0, -sin(angle), cos(angle), 0),
        simd_float4(0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeRotationYMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), 0, sin(angle), 0),
        simd_float4( 0,          1,          0, 0),
        simd_float4(-sin(angle), 0, cos(angle), 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeRotationZMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), sin(angle), 0, 0),
        simd_float4(-sin(angle), cos(angle), 0, 0),
        simd_float4( 0,          0,          1, 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeScaleMatrix(xScale: Float, yScale: Float, zScale: Float) -> simd_float4x4 {
    let rows = [
        simd_float4(xScale,      0,      0, 0),
        simd_float4(     0, yScale,      0, 0),
        simd_float4(     0,      0, zScale, 0),
        simd_float4(     0,      0,      0, 1)
    ]
    return float4x4(rows: rows)
}

func printM(_ t: simd_float4x4) {
    let t = t.transpose
    printRow(t.columns.0)
    printRow(t.columns.1)
    printRow(t.columns.2)
    printRow(t.columns.3)
}

func printRow(_ row: simd_float4) {
    print("[\(row.x),\t\(row.y),\t\(row.z),\t\(row.w)]")
}

extension SCNVector3 {
    
    /// Distance to vector
    /// - Parameter vector: the vector
    /// - Returns: distance
    func distance(to vector: SCNVector3) -> Float {
        return simd_distance(simd_float3(self), simd_float3(vector))
    }
    
    func translation(to vector: SCNVector3) -> simd_float4x4 {
        return makeTranslationMatrix(tx: Float(vector.x - x), ty: Float(vector.y - y), tz: Float(vector.z - z))
    }
    
    func center(between point: SCNVector3) -> SCNVector3 {
        return SCNVector3(x: x + (point.x - x)/2, y: y + (point.y - y)/2, z: z + (point.z - z)/2)
    }
    
    /// Convert to simd_float4
    func toSimd4() -> simd_float4 {
        return simd_float4(x: x, y: y, z: z, w: 1)
    }
}

extension SCNVector3 {
    
    /// Angle of rotation in XZ plane
    /// - Parameter point: the point
    /// - Returns: angle
    func angleXZ(to point: SCNVector3) -> CGFloat {
//        let dx = point.x - x
//        let dz = point.z - z // because in 3D X and Z are clockwise
//        if dx == 0 {
//            return dz < 0 ? .pi / 2 : -.pi / 2
//        }
//        let v = -CGFloat(atan(dz/dx))
//        return 0//dx < 0 ? .pi + v : v
        let dx = point.x - x
        let dz = -1 * (point.z - z) // because in 3D X and Z are clockwise
        if dx == 0 {
            return dz > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(dz/dx) + (dx < 0 ? .pi : 0))
    }
    
    /// Angle of projection to plane crossing p1 and parallel to XZ
    /// - Parameter point: the point
    /// - Returns: angle
    func angleXZProjection(to point: SCNVector3) -> CGFloat {
        let h = point.y - y
        let proj = SCNVector3(point.x, y, point.z)
        let d = proj.distance(to: self)
        if d == 0 {
            return h > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(h/d))
    }
    
    func xy2xz() -> SCNVector3 {
        return SCNVector3(x, z, y)
    }
}

extension CGFloat {
    
    func radToG() -> CGFloat {
        return self / .pi * 180
    }
    
    func toRad() -> CGFloat {
        return self / 180 * .pi
    }
}
extension matrix_float4x4 {
    /**
     Calculate position vector of a pose matrix
     
     - Returns: A SCNVector3 from the translation components of the matrix
     */
    public func position() -> SCNVector3 {
        return SCNVector3(columns.3.x, columns.3.y, columns.3.z)
    }
}
extension simd_float4 {
    
    /// Convert to simd_float3
    func toSimd3() -> simd_float3 {
        return simd_float3(x: x, y: y, z: z)
    }
    
    var vector: SCNVector3 {
        return SCNVector3(x, y, z)
    }
}

extension simd_float3 {
    
    func distance() -> Float {
        return simd_distance(self, simd_float3.init())
    }
}
// --------------------------------------------------------------------
// Materials
let mRed = SCNMaterial(); mRed.isDoubleSided = true; mRed.diffuse.contents = Color.red.withAlphaComponent(0.8)
let mGreen = SCNMaterial(); mGreen.isDoubleSided = true; mGreen.diffuse.contents = Color.green.withAlphaComponent(0.8)
let mYellow = SCNMaterial(); mYellow.isDoubleSided = true; mYellow.diffuse.contents = Color.yellow.withAlphaComponent(0.8)
let mBlack = SCNMaterial(); mBlack.isDoubleSided = true; mBlack.diffuse.contents = Color.black.withAlphaComponent(0.8)

func addPoint(_ position: SCNVector3, radius: CGFloat = 0.2, color: UIColor = .black) {
    let m = SCNMaterial(); m.isDoubleSided = true; m.diffuse.contents = color.withAlphaComponent(0.8)
    
    let shape = SCNSphere(radius: radius)
    shape.materials = [m]
    let node = SCNNode(geometry: shape)
    node.position = position
    scene.rootNode.addChildNode(node)
}

// --------------------------------------------------------------------
// Coordinate system
/// Show line between points using SCNBox
func showLineBetweenPoints(p1: SCNVector3, p2: SCNVector3, width: CGFloat) {
    let distance = p1.distance(to: p2)
    
    let shape = SCNBox(width: 1, height: width, length: width, chamferRadius: 0)
    shape.materials = [mRed]
    let node = SCNNode(geometry: shape)
    
    let angleXZProj = -1 * p1.angleXZProjection(to: p2)
    let rotationZ = makeRotationZMatrix(angle: Float(angleXZProj)) // dodo optimize using a/b = sin()
    let scale = makeScaleMatrix(xScale: distance, yScale: 1, zScale: 1)
    let angle = p1.angleXZ(to: p2)
    let rotationY = makeRotationYMatrix(angle: Float(angle))
    let translation = SCNVector3().translation(to: p1.center(between: p2))
    print("\nscale:")
    printM(scale)
    print("\nrotation:")
    printM(rotationY)
    print("\ntranslation:")
    printM(translation)
    node.simdTransform = matrix_identity_float4x4 * translation * rotationY * rotationZ * scale
    //node.simdTransform = translation * matrix_identity_float4x4
    scene.rootNode.addChildNode(node)
}

/// Show coordinate system defined by `transform`
func showCoordinateSystem(_ transform: simd_float4x4, lendth: CGFloat = 5) {
    let axisLength = lendth
    let xLine = SCNBox(width: axisLength, height: 0.1, length: 0.1, chamferRadius: 0)
    xLine.materials = [mRed]
    let xLineNode = SCNNode(geometry: xLine)
    xLineNode.position = SCNVector3(axisLength / 2, 0, 0)
    scene.rootNode.addChildNode(xLineNode)
    
    let yLine = SCNBox(width: 0.1, height: axisLength, length: 0.1, chamferRadius: 0)
    yLine.materials = [mGreen]
    let yLineNode = SCNNode(geometry: yLine)
    yLineNode.position = SCNVector3(0, axisLength / 2, 0)
    scene.rootNode.addChildNode(yLineNode)
    
    let zLine = SCNBox(width: 0.1, height: 0.1, length: axisLength, chamferRadius: 0)
    zLine.materials = [mYellow]
    let zLineNode = SCNNode(geometry: zLine)
    zLineNode.position = SCNVector3(0, 0, axisLength / 2)
    scene.rootNode.addChildNode(zLineNode)

    // Transform
    xLineNode.simdTransform = transform * xLineNode.simdTransform
    yLineNode.simdTransform = transform * yLineNode.simdTransform
    zLineNode.simdTransform = transform * zLineNode.simdTransform
}

// Calculate projection from camera transform (THIS IS WRONG. It's just projection of any other transform, not camera)
func cameraProjectionTransform(_ camera: simd_float4x4) -> simd_float4x4 {
    let ox = simd_float4(x: 1, y: 0, z: 0, w: 1)
    let cameraOZVectorEnd = camera * ox
    let c1 = camera.position()
    let c2 = SCNVector3(cameraOZVectorEnd.toSimd3())
    let proC1 = SCNVector3(c1.x, 0, c1.z)
    let proC2 = SCNVector3(c2.x, 0, c2.z)
    let angleC = proC1.angleXZ(to: proC2)
    let translateC = makeTranslationMatrix(tx: c1.x, ty: 0, tz: c1.z)
    let rotationC = makeRotationYMatrix(angle: Float(angleC)) // dodo optimize using a/b = sin()
    let cameraProjection = translateC * rotationC
    return cameraProjection
}

// Calculate projection from camera transform. Use Oz axis as reference, and calculate rotation along Y
func cameraProjectionTransform2(_ camera: simd_float4x4) -> simd_float4x4 {
    let oR = simd_float4(x: 0, y: 0, z: 1, w: 1) // reference axis - Z
    let cameraORVectorEnd = camera * oR
    let c1 = camera.position()
    let c2 = SCNVector3(cameraORVectorEnd.toSimd3())
    let proC1 = SCNVector3(c1.x, 0, c1.z)
    let proC2 = SCNVector3(c2.x, 0, c2.z)
    let angleC = proC1.angleXZ(to: proC2) + .pi / 2
    let translateC = makeTranslationMatrix(tx: c1.x, ty: 0, tz: c1.z)
    let rotationC = makeRotationYMatrix(angle: Float(angleC)) // dodo optimize using a/b = sin()
    let cameraProjection = translateC * rotationC
    return cameraProjection
}

// This is added later and seems it's the same as cameraProjectionTransform2, but adds position
/// Creates transform that makes space be oriented to the camera and positioned as `initialTransform`
func updateYRotationFromCamera(_ camera: simd_float4x4, forTransform initialTransform: simd_float4x4) -> simd_float4x4 {
    let oR = simd_float4(x: 0, y: 0, z: 1, w: 1) // reference axis - Z
    let cameraORVectorEnd = camera * oR
    let c1 = camera.position()
    let c2 = SCNVector3(cameraORVectorEnd.toSimd3())
    let proC1 = SCNVector3(c1.x, 0, c1.z)
    let proC2 = SCNVector3(c2.x, 0, c2.z)
    let angleC = proC1.angleXZ(to: proC2) + .pi / 2
    let translateC = makeTranslationMatrix(tx: initialTransform.position().x, ty: initialTransform.position().y, tz: initialTransform.position().z)
    let rotationC = makeRotationYMatrix(angle: Float(angleC))
    let cameraProjection = translateC * rotationC
    return cameraProjection
}

/// Create transform by two points. Ox will start from `p1` and go through `p2`.
func createTransformXByTwoPoints(p1: SCNVector3, p2: SCNVector3) -> simd_float4x4{
    let angleXZ = p1.angleXZ(to: p2)
    let angleXZProj = -1 * p1.angleXZProjection(to: p2)
    print(angleXZProj.radToG())
    let translate = makeTranslationMatrix(tx: p1.x, ty: p1.y, tz: p1.z)
    let rotationY = makeRotationYMatrix(angle: Float(angleXZ)) // dodo optimize using a/b = sin()
    let rotationZ = makeRotationZMatrix(angle: Float(angleXZProj)) // dodo optimize using a/b = sin()
    let d = p1.distance(to: p2)
    let scale = makeScaleMatrix(xScale: d, yScale: d, zScale: d)
    let transform = translate * rotationY * rotationZ * scale
    return transform
}

// Calculate 3rd point of a rectangle (square) defined by two points (left top and right bottom). It will be top right point.
// Also it scales so that 3rd rectangle point matches initial (1,1) coordinates
// If points have equal X and Z, then transform will be created so, than Ox axis will go through these points
func createTransformByRectanglePoints(point1: SCNVector3, point3: SCNVector3) -> simd_float4x4 {
    let p1 = point1
    let p2 = point3
    let projXZ: simd_float4 = simd_float4(x: p2.x - p1.x, y: 0, z: p2.z - p1.z, w: 0) // w:0 - it's a vector, not point
//    let rotate90: simd_float4 = makeRotationYMatrix(angle: -.pi/2) * projXZ
    let rotate270: simd_float4 = makeRotationYMatrix(angle: .pi/2) * projXZ
    let center = p1.center(between: p2)
    let d1 = rotate270.toSimd3().distance()
    let d2 = p1.distance(to: center)
    if d1 != 0 {
        let k = d2 / d1
        let v3 = simd_float4(x: rotate270.x * k, y: rotate270.y * k, z: rotate270.z * k, w: 1)
        let point3: simd_float3 = (makeTranslationMatrix(tx: center.x, ty: center.y, tz: center.z) * v3).toSimd3()
        let p3 = SCNVector3(point3)
        // addPoint(p3)
        
//        let v4 = simd_float4(x: rotate90.x * k, y: rotate90.y * k, z: rotate90.z * k, w: 1)
//        let point4: simd_float3 = (makeTranslationMatrix(tx: center.x, ty: center.y, tz: center.z) * v4).toSimd3()
//        let p4 = SCNVector3(point4)
//         addPoint(p4)
        
        let t = createTransformXByTwoPoints(p1: p1, p2: p3)
        return t
    }
    else {
       return createTransformXByTwoPoints(p1: p1, p2: p2)
    }
}

showCoordinateSystem(matrix_identity_float4x4)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------------------

func testPathLineDrawing() {
    // Sample points
    let p1 = SCNVector3(x: 3, y: 0, z: 2)
    let p2 = SCNVector3(x: 3+4, y: 2, z: 2+1)
    addPoint(p1)
    addPoint(p2)
    
    // --------------------------------------------------------------------
    // Line between points (using a rectangle)
    showLineBetweenPoints(p1: p1, p2: p2, width: 0.3)
}

// --------------------------------------------------------------------
// Sample camera coordinate system. It makes projection so that Oy goes up.
func testSampleCameraTransform() {
    let rX = makeRotationXMatrix(angle: Float(CGFloat(20).toRad()))
    let rY = makeRotationYMatrix(angle: Float(CGFloat(-45).toRad()))
    let camRotation = rX * rY // first Oy rotation, then Ox.
    let camTranslation = SCNVector3().translation(to: SCNVector3(3, 1, 5))
    let camera = camTranslation * camRotation
    
    // Show desired camera's coord. system projection to XZ
    //showCoordinateSystem(SCNVector3().translation(to: SCNVector3(3, 0, 5)) * rY, lendth: 2)
    
    // Calculate projection from camera transform
    // Base for `cameraProjectionTransform()`
    //let ox = simd_float4(x: 1, y: 0, z: 0, w: 1)
    //let cameraOZVectorEnd = camera * ox
    //let c1 = camera.position()
    //let c2 = SCNVector3(cameraOZVectorEnd.toSimd3())
    ////addPoint(c1)
    ////addPoint(c2)
    //let proC1 = SCNVector3(c1.x, 0, c1.z)
    //let proC2 = SCNVector3(c2.x, 0, c2.z)
    ////addPoint(proC1)
    ////addPoint(proC2)
    //let angleC = proC1.angleXZ(to: proC2)
    //let translateC = makeTranslationMatrix(tx: c1.x, ty: 0, tz: c1.z)
    //let rotationC = makeRotationYMatrix(angle: Float(angleC))
    //let t1 = rotationC * ox     // rotation transform
    ////addPoint(SCNVector3(t1.toSimd3()))
    //let cameraProjection = translateC * rotationC
    let cameraProjection = cameraProjectionTransform(camera)
    
    /// Output:
    // Show camera's coord. system
    showCoordinateSystem(camera, lendth: 3)
    showCoordinateSystem(cameraProjection, lendth: 3)
}

//---------------------------------------------------------------------------
// Example of real camera transform. It makes projection to "match" real coordinate system in a way the axises go. The real camera tranform has different axises - X and Y are screen coordiantes, while Oz goes into the camera. Real coordinates have Oy doing up.
func testRealCameraTransform() {
    //let camera2 = simd_float4x4([[0.16275628, -0.98273903, 0.08794455, 0.0], [0.582302, 0.023716599, -0.8126266, 0.0], [0.7965141, 0.18347037, 0.5761109, 0.0], [-0.007652493, -0.09870171, 0.14205848, 0.99999994]])
    let camera2 = simd_float4x4([[0.13102177, -0.70584244, 0.69614637, 0.0], [0.9783871, -0.021248987, -0.20568728, 0.0], [0.15997522, 0.7080501, 0.68780303, 0.0], [-0.084827706, 0.319571, -0.09356433, 1.0]])
    let cameraProjection2 = cameraProjectionTransform2(camera2)
    
    /// Output:
    showCoordinateSystem(camera2, lendth: 5)
    showCoordinateSystem(cameraProjection2, lendth: 2)
}

/// Shows real camera tranform and points in all 1/8-spaces that are visible for the camera.
func testRealCameraTransformPreProjectionCheck() {
    //let camera2 = simd_float4x4([[0.16275628, -0.98273903, 0.08794455, 0.0], [0.582302, 0.023716599, -0.8126266, 0.0], [0.7965141, 0.18347037, 0.5761109, 0.0], [-0.007652493, -0.09870171, 0.14205848, 0.99999994]])
    let camera2 = simd_float4x4([[0.13102177, -0.70584244, 0.69614637, 0.0], [0.9783871, -0.021248987, -0.20568728, 0.0], [0.15997522, 0.7080501, 0.68780303, 0.0], [-0.084827706, 0.319571, -0.09356433, 1.0]])
    let cameraProjection2 = cameraProjectionTransform2(camera2)
    
    /// Output:
    showCoordinateSystem(camera2)
    showCoordinateSystem(cameraProjection2, lendth: 2)
    
    let points = [
        SCNVector3(1, 1, 1),
        SCNVector3(1, 1, -1),
        SCNVector3(1, -1, 1),
        SCNVector3(1, -1, -1),
        SCNVector3(-1, 1, 1),
        SCNVector3(-1, 1, -1),
        SCNVector3(-1, -1, 1),
        SCNVector3(-1, -1, -1)
    ]
    
    let cameraInverseTransform = camera2.inverse
    for p in points {
        // Convert point coordinates into camera coordinate space
        let cameraTransformPoint = cameraInverseTransform * p.toSimd4()
        print("\(p) -> \(cameraTransformPoint)")
        if cameraTransformPoint.z > 0 {
            addPoint(p)
        }
    }
}

//---------------------------------------------------------------------------
//// Testing .angleXZ method
func testAngleXZ() {
    let dz = sin(CGFloat(30).toRad())
    let dx = cos(CGFloat(30).toRad())
    SCNVector3().angleXZ(to: SCNVector3(dx, 0, dz)).radToG()
    SCNVector3().angleXZ(to: SCNVector3(dx, 0, -dz)).radToG()
    SCNVector3().angleXZ(to: SCNVector3(-dx, 0, dz)).radToG()
    SCNVector3().angleXZ(to: SCNVector3(-dx, 0, -dz)).radToG()
    
    sin(CGFloat(30).toRad())
    cos(CGFloat(30).toRad())
    tan(CGFloat(30).toRad())
    atan(dz/dx).radToG()
    
    sin(CGFloat(90+30).toRad())
    cos(CGFloat(90+30).toRad())
    tan(CGFloat(90+30).toRad())
    
    sin(CGFloat(180+30).toRad())
    cos(CGFloat(180+30).toRad())
    tan(CGFloat(180+30).toRad())
}

func testAngleXZProjection() {
    let h = sin(CGFloat(30).toRad())
    let d = cos(CGFloat(30).toRad())
    SCNVector3().angleXZProjection(to: SCNVector3(d, h, 0)).radToG()
    SCNVector3().angleXZProjection(to: SCNVector3(d, -h, 0)).radToG()
    SCNVector3().angleXZProjection(to: SCNVector3(-d, h, 0)).radToG()
    SCNVector3().angleXZProjection(to: SCNVector3(-d, -h, -0)).radToG()
}
// -----------------------------------------------------------------------------
// Creates new coordinate system based on two points
func testNewCoordinatesOnTwoPoints() {
    let p1 = SCNVector3(x: 0, y: 1, z: 0)
    let p2 = SCNVector3(x: 1, y: 0, z: -1)
    addPoint(p1)
    addPoint(p2)
    
    let t = createTransformXByTwoPoints(p1: p1, p2: p2)
    showCoordinateSystem(t, lendth: 2)
}
// Creates new coordinate system based on two points, and add vector going from camera
func testCoordinatesOnTwoPointsAndCameraVector() {
    let p1 = SCNVector3(x: 0, y: 1, z: 0)
    let p2 = SCNVector3(x: 1, y: 0, z: -1)
//    addPoint(p1)
//    addPoint(p2)
    
    let t = createTransformXByTwoPoints(p1: p1, p2: p2)
    showCoordinateSystem(t, lendth: 2)
    
    // Vector going from the camera
    let camVector = SCNVector3(x: 0, y: 0, z: 1) // Z
    let camVectorPoint = t * camVector.toSimd4()
    addPoint(camVectorPoint.vector, color: .red)
}

// Two points define two corners of the rectangle. It's used to create tranform which have X and Z axis parallel to sides of the rectangle
// There is minor issue - OZ will not go through 4rd point. TODO: to fix it - need to rotate along Ox too.
func testRectangleBasedTranfrom() {
    
    let p1 = SCNVector3(x: 0, y: 1, z: 0)
    let p2 = SCNVector3(x: 1, y: 0, z: 1)
    addPoint(p1)
    addPoint(p2)
    
    let t = createTransformByRectanglePoints(point1: p1, point3: p2)
    showCoordinateSystem(t, lendth: 2)
}

func testRectangleBetweenPoints() {
    // Sample points
    let p1 = SCNVector3(x: 3, y: 0, z: 2)
    let p2 = SCNVector3(x: 3+4, y: 0, z: 2+1)
    addPoint(p1)
    addPoint(p2)
    
    func translatePath(path: [SCNVector3], point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let t = createTransformByRectanglePoints(point1: point1, point3: point2)
        
        //showCoordinateSystem(t, lendth: 2)
        let points = path
            .map({$0.xy2xz()})
            .map({t * simd_float4(x: $0.x, y: $0.y, z: $0.z, w: 1)})
            .map({SCNVector3($0.toSimd3())})
        return points
    }
    
    func createPathReactangle(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path: [SCNVector3] = [
            SCNVector3(0, 0, 0),
            SCNVector3(1, 0, 0),
            SCNVector3(1, 1, 0),
            SCNVector3(0, 1, 0),
            SCNVector3(0, 0, 0)
        ]
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    func createPathCheckmark(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path: [SCNVector3] = [
            SCNVector3(0, 0.5, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(1, 0, 0)
        ]
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    /// Create path that fits into [0,0], [1,1] and reprecent an arrow. Points are used to calculate size of the arrow ending
    func createInitialPathArrow(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let sizeOfEndInPercent = 0.25
        let path: [SCNVector3] = [
            SCNVector3(0.5 - sizeOfEndInPercent, 1 - sizeOfEndInPercent, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(0.5, 0, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(0.5 + sizeOfEndInPercent, 1 - sizeOfEndInPercent, 0)
        ]
        return path
    }
    
    func rotate(path: [SCNVector3], angle: Float) -> [SCNVector3] {
        let t1 = makeTranslationMatrix(tx: -0.5, ty: -0.5, tz: 0)
        let r = makeRotationZMatrix(angle: angle)
        let t2 = makeTranslationMatrix(tx: 0.5, ty: 0.5, tz: 0)
        let t: simd_float4x4 = t2 * r * t1
        return path
            .map({t * simd_float4(x: $0.x, y: $0.y, z: $0.z, w: 1)})
            .map({SCNVector3($0.toSimd3())})
    }
    
    func createPathArrowUp(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    func createPathArrowLeft(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi * 3 / 2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    func createPathArrowRight(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi / 2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    func createPathArrowDown(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path = createInitialPathArrow(point1: point1, point2: point2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    /// Show line between points using SCNBox
    func showPath(path: [SCNVector3], width: CGFloat) {
        var last: SCNVector3?
        for point in path {
            addPoint(point, color: UIColor.red.withAlphaComponent(0.2))
            if let last = last {
                showLineBetweenPoints(p1: last, p2: point, width: width)
            }
            last = point
        }
    }
    
//    showPath(path: createPathReactangle(point1: p1, point2: p2), width: 0.2) // Rectangle
//    showPath(path: createPathCheckmark(point1: p1, point2: p2), width: 0.2) // Checkmark
    showPath(path: createPathArrowLeft(point1: p1, point2: p2), width: 0.2) // Arrow
}
//////////////////////////////////////////////////////////////////////////////////////
/// Testing
//testRectangleBetweenPoints()
testRealCameraTransform()
//testRealCameraTransformPreProjectionCheck()
//testNewCoordinatesOnTwoPoints()
//testCoordinatesOnTwoPointsAndCameraVector()
//testPathLineDrawing()
//testAngleXZ()

//////////////////////////////////////////////////////////////////////////////////////
// post to https://stackoverflow.com/questions/49850591/swift-arkit-place-a-rectangle-based-on-2-nodes


