//
//  SCNVector+Extensions.swift
//  SwiftEx83/ARKit
//
//  Created by Volkov Alexander on 09/29/2020.
//  Copyright (c) 2020 Alexander Volkov. All rights reserved.
//

import ARKit

extension SCNVector3 {
    
    /// Distance to vector
    /// - Parameter vector: the vector
    /// - Returns: distance
    public func distance(to vector: SCNVector3) -> Float {
        return simd_distance(simd_float3(self), simd_float3(vector))
    }
    
    /// Distance from projection to projection (on XZ)
    /// - Parameter vector: the vector
    /// - Returns: distance
    public func distanceOnXZ(to vector: SCNVector3) -> Float {
        return simd_distance(simd_float3(x: self.x, y: 0, z: self.z), simd_float3(x: vector.x, y: 0, z: vector.z))
    }
    
    /// Translation matrix to given point from current
    /// - Parameter point: the point
    public func translation(to point: SCNVector3) -> simd_float4x4 {
        return makeTranslationMatrix(tx: Float(point.x - x), ty: Float(point.y - y), tz: Float(point.z - z))
    }
    
    /// Center between current and given point
    /// - Parameters:
    ///   - point: the point
    ///   - percent: the percent of the line from p1 to p2
    public func center(between point: SCNVector3, percent: Float = 0.5) -> SCNVector3 {
        return SCNVector3(x: x + (point.x - x) * percent, y: y + (point.y - y) * percent, z: z + (point.z - z) * percent)
    }
    
    /// Angle on XZ plane between vector (self -> point) and (0,0)->(0,-1). Used for showing minimap projection of the world.
    /// - Parameter point: the point
    /// - Returns: angle
    public func angleOnXZ(to point: SCNVector3) -> CGFloat {
        let dx = point.x - x
        let dz = -1 * (point.z - z) // because in 3D X and Z are clockwise
        if dx == 0 {
            return dz > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(dz/dx) + (dx < 0 ? .pi : 0))
    }
    
    /// Angle of between (self->point) and it's projection to XZ.
    /// - Parameter point: the point
    /// - Returns: angle
    public func angleWithXZProjection(to point: SCNVector3) -> CGFloat {
        let h = point.y - y
        let proj = SCNVector3(point.x, y, point.z)
        let d = proj.distance(to: self)
        if d == 0 {
            return h > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(h/d))
    }
    
    /// Calcularte angle between vectors in XZ space in [-180, 180] limits
    /// - Parameter vector: the second vector
    /// - Returns: angle
    public func angleOnXZOriginal180(to vector: SCNVector3) -> Float {
        let a1 = SCNVector3(0, 0, 0).angleOnXZ(to: self)
        let a2 = SCNVector3(0, 0, 0).angleOnXZ(to: vector)
        var a = Float(a2 - a1)
        while a > .pi {
            a = a - .pi * 2
        }
        while a < -.pi {
            a = a + .pi * 2
        }
        return a
    }
    
    /// Angle to the vector
    /// - Parameter vector: the vector
    /// - Returns: the angle between two vectors:
    public func angle(to vector: SCNVector3) -> Float {
        let d1 = distance(to: SCNVector3())
        let d2 = vector.distance(to: SCNVector3())
        guard d1 != 0 && d2 != 0 else { return 0 }
        return acos((x * vector.x + y * vector.y) / (d1 * d2))
    }
}

extension SCNVector3 {
    
    /// Convert to simd_float3
    public func toSimd3() -> simd_float3 {
        return simd_float3(self)
    }
    /// Convert to simd_float4
    public func toSimd4() -> simd_float4 {
        return simd_float4(x: x, y: y, z: z, w: 1)
    }
}
