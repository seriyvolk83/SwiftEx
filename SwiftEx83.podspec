Pod::Spec.new do |s|
  s.name        = "SwiftEx83"
  s.version     = "1.1.12"
  s.summary     = "Contains Foundation and UIKit extensions that are very helpful in every project."
  s.homepage    = "https://gitlab.com/seriyvolk83/SwiftEx"
  s.license     = { :type => "MIT" }
  s.authors     = { "seriyvolk83" => "volk@frgroup.ru" }

  s.requires_arc = true
  s.swift_version = "5.2"
  s.ios.deployment_target = "10.0"
  s.source   = { :git => "https://gitlab.com/seriyvolk83/SwiftEx.git", :tag => s.version }
  s.source_files = "Source/*.swift"

  s.default_subspec = "Data"

  s.subspec 'Core' do |cs|
    cs.source_files = "Source/*.swift"
  end

  s.subspec 'Data' do |cs|
    cs.dependency 'SwiftyJSON', '~> 4.0'
    cs.source_files = "Source/*.swift", "SourceData/*.swift"
  end

  s.subspec 'UI' do |cs|
    cs.dependency 'SwiftyJSON', '~> 4.0'
    cs.source_files = "Source/*.swift", "SourceData/*.swift", "SourceUI/*.swift"
  end
  
  s.subspec 'Api' do |cs|
      cs.dependency 'SwiftyJSON', '~> 4.0'
      cs.source_files = "Source/*.swift", "SourceData/*.swift", "SourceApi/*.swift"
  end

  s.subspec 'Int' do |cs|
      cs.dependency 'RxAlamofire', '~> 5.0.0'
      cs.dependency 'SwiftyJSON', '~> 4.0'
      cs.dependency 'NSObject+Rx'
      cs.source_files = "Source/*.swift", "SourceData/*.swift", "SourceUI/*.swift", "SourceApi/*.swift", "SourceInt/*.swift"
  end
  
  s.subspec 'ARKit' do |cs|
    cs.source_files = "SourceARKit/*.swift"
  end
end
