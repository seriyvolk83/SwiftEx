//
//  ServerExtensions.swift
//  SampleServer
//
//  Created by Volkov Alexander on 03.11.2018.
//  Updated by Volkov Alexander on 10/12/2019.
//

import Foundation
import PerfectHTTP
import SwiftyJSON
import SwiftExApi

//////////////

// Content-Type constant
public let applicationJson = "application/json"

/// Helpful extension for HTTPResponse
extension HTTPResponse {
    
    /// Add message to response
    ///
    /// - Parameter message: the message
    public func message(_ message: String) {
        let json = JSON(["message": message])
        appendBody(string: json.rawString() ?? "")
    }
    
    /// Add response
    ///
    /// - Parameter dic: the dictionary
    public func response(_ dic: [String: Any]) {
        let json = JSON(dic)
        appendBody(string: json.rawString() ?? "")
    }
    
    /// Add response
    ///
    /// - Parameter array: the array
    public func response(_ array: [Any]) {
        setHeader(.contentType, value: applicationJson)
        let json = JSON(array)
        appendBody(string: json.rawString() ?? "")
    }
    
    /// Set the status and call completed() and add message to response
    ///
    /// - Parameters:
    ///   - status: the status
    ///   - message: the message
    public func completed(status: HTTPResponseStatus, message msg: String) {
        setHeader(.contentType, value: applicationJson)
        let error = SharedServerErrorFactory.createError(status: status, msg)
        if error is String {
            self.message(msg) // dodo format as error
        }
        else {
            try? self.response(error)
        }
        self.status = status
        completed()
    }
    public func completed(status: HTTPResponseStatus, error: ServerError) {
        setHeader(.contentType, value: applicationJson)
        do {
            try self.response(error)
        }
        catch let error {
            print("ERROR: \(error)")
            try? self.response(["type": "ERROR", "description": "Unknown error: \(error)"])
        }
        self.status = status
        completed()
    }
    
    /// Add response
    ///
    /// - Parameter object: the object
    public func response(_ object: Encodable) throws {
        setHeader(.contentType, value: applicationJson)
        let json = try object.json()
        appendBody(string: json.rawString() ?? "")
    }
}

/// Helpful extension for HTTPRequest
extension HTTPRequest {
    
    /// Get ID from the path
    ///
    /// - Returns: the ID
    /// - Throws: 400 or 404
    public func getUUID() throws -> UUID {
        guard let id = self.urlVariables["id"] else {
            throw HTTPResponseError(status: .badRequest, description: "ID is not provided")
        }
        guard let uuid = UUID(uuidString: id) else { throw HTTPResponseError(status: .notFound, description: "Object not found") }
        return uuid
    }
    
    /// Get ID from the path
    ///
    /// - Returns: the ID
    /// - Throws: 400 or 404
    public func getId() throws -> IdType {
        guard let idString = self.urlVariables["id"] else {
            throw HTTPResponseError(status: .badRequest, description: "ID is not provided")
        }
        guard let id: IdType = idString.toIdType() else { throw HTTPResponseError(status: .notFound, description: "Object not found") }
        return id
    }
    
    /// Parse JSON body
    ///
    /// - Returns: the JSON
    public func json() -> JSON? {
        let json = JSON(parseJSON: postBodyString ?? "")
        if json == JSON.null {
            return nil
        }
        else {
            return json
        }
    }
    
    /// Parse body and return object
    public func body<T: Decodable>() throws -> T {
        guard let body = self.json() else { throw HTTPResponseError(status: .badRequest, description: "Incorrect JSON format") }
        do {
            let object: T = try body.decode(T.self)
            return object
        }
        catch let error {
            throw HTTPResponseError(status: .badRequest, description: "Incorrect parameter: \(error)")
        }
    }
}

/// Function which receives request and response objects and generates content.
public typealias ThrowRequestHandler = (HTTPRequest, HTTPResponse) throws -> ()

/// Handler wrapper. Catches server errors
open class HandlerWrapper {
    
    /// the effective handler
    public let handler: ThrowRequestHandler
    
    /// Initializer
    ///
    /// - Parameter handler: the handler to wrap
    public init(_ handler: @escaping ThrowRequestHandler) {
        self.handler = handler
    }
    
    /// RequestHandler
    ///
    /// - Parameters:
    ///   - request: the request
    ///   - response: the response
    public func handler(request: HTTPRequest, response: HTTPResponse) {
        do {
            try handler(request, response)
        }
        catch let error {
            if let error = error as? HTTPResponseError {
                response.completed(status: error.status, message: error.description)
            }
            else { // catch error and return 500 code with error message
                response.completed(status: .internalServerError, message: error.localizedDescription)
            }
        }
    }
}

extension Routes {
    
    /// Add the given uri and handler as a route.
    /// This will add the route for all standard methods.
    public mutating func addCRUD(uri: String, handler: @escaping ThrowRequestHandler) {
        add(method: .get, uri: uri, handler: HandlerWrapper(handler).handler)
        add(method: .post, uri: uri, handler: HandlerWrapper(handler).handler)
        add(method: .put, uri: "\(uri)/{id}", handler: HandlerWrapper(handler).handler)
        add(method: .delete, uri: "\(uri)/{id}", handler: HandlerWrapper(handler).handler)
        
        add(method: .patch, uri: "\(uri)/{id}", handler: HandlerWrapper(handler).handler)
        add(method: .patch, uri: uri, handler: HandlerWrapper(handler).handler)
    }
}

extension String {
    
    public func toIdType() -> Int? {
        return Int(self)
    }
    
    public func toIdType() -> String? {
        return self
    }
}
