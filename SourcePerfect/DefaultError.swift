//
//  DefaultError.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 29.10.2019.
//

import Foundation
import PerfectHTTP

/**
 If errors should be different (different format for output), then you need to
 create new class with ServerError protocol and new class with ServerErrorFactory protocol.
 Then update the value in ServerErrorFactoryShared with new factory instance.
 
 To throw errors client should use either `HTTPResponseError` with status directly (recommended)
 or `SharedServerErrorFactory.createError(..)`
 */
// Protocol for server errors
public protocol ServerError: Encodable {
}

/// The default error to throw
public struct DefaultError: ServerError {
    
    // the fields
    public var type: String
    public var description: String
    
    // Get "ERROR"
    public static func error(_ string: String) -> DefaultError {
        return DefaultError(type: "ERROR", description: string)
    }
    
    // Get "ERROR_PARAMETER"
    public static func parameter(_ string: String) -> DefaultError {
        return DefaultError(type: "ERROR_PARAMETER", description: string)
    }
    
    // Get "ERROR_BODY"
    public static func body(_ string: String) -> DefaultError {
        return DefaultError(type: "ERROR_BODY", description: string)
    }
    
    // "Wrong body format" error
    public static let wrongBody = DefaultError.body("Wrong body format")
}

public protocol ServerErrorFactory {
    
    func createError(_ message: String) -> ServerError
    
    func createError(status: HTTPResponseStatus, _ message: String) -> ServerError
}

// the main factory for creating server errors
public var SharedServerErrorFactory: ServerErrorFactory = DefaultServerErrorFactory()

// Default server error factory
open class DefaultServerErrorFactory: ServerErrorFactory {
    
    public func createError(_ message: String) -> ServerError {
        return DefaultError(type: "ERROR", description: message)
    }
    
    public func createError(status: HTTPResponseStatus, _ message: String) -> ServerError {
        return DefaultError(type: status.description, description: message)
    }
}

