//
//  Filter404.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 27.10.2019.
//

//import PerfectLib
//import PerfectHTTP
//import PerfectHTTPServer
import PerfectHTTP

// Example of HTTPResponseFilter.
public struct Filter404: HTTPResponseFilter {
    
    // Updates message for 404 responses
    public func filterHeaders(response: HTTPResponse, callback: (HTTPResponseFilterResult) -> ()) {
        if case .notFound = response.status {
            response.bodyBytes.removeAll()
            let string = "We cannot find file \(response.request.path).\n"
            response.setBody(string: string)
            response.setHeader(.contentLength, value: "\(string.count)")
            callback(.done)
        }
        else {
            callback(.continue)
        }
    }
    
    // Try `curl -v http://localhost:8080/nothing`
    public func filterBody(response: HTTPResponse, callback: (HTTPResponseFilterResult) -> ()) {
        callback(.continue)
    }
    
}

