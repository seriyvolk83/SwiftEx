//
//  DefaultHandler.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 29.10.2019.
//

import PerfectHTTP
import SwiftyJSON
import SwiftExData

// Usage:
/// ```
/// var routes = Routes(baseUri: "/api/1.0")
/// routes.addDefaultRoutes(listResult: EmptyPageV3, validateJsonHeader: false)
/// ```
extension Routes {
    
    // Add default routes for methods: GET, POST, DELETE, PUT, PATCH
    ///
    /// - Parameters:
    ///   - listResult: the object to return for any GET request
    ///   - validateJsonHeader: true - will validate "Content-type" header for "application/json" for POST, PUT, PATCH requests
    public mutating func addDefaultRoutes(listResult: Encodable? = nil, validateJsonHeader: Bool = false) {
        var handler = DefaultHandler()
        handler.listResult = listResult
        handler.validateJsonHeader = validateJsonHeader
        
        self.add(method: .get, uri: "/**", handler: handler.get())
        self.add(method: .post, uri: "/**", handler: handler.post())
        self.add(method: .delete, uri: "/**", handler: DefaultHandler.delete)
        self.add(method: .put, uri: "/**", handler: handler.put)
        self.add(method: .patch, uri: "/**", handler: handler.patch)
    }
}

// Sample empty lists
public let EmptyPageV1: PageV1<String> = PageV1<String>(items: [], offset: nil)
public let EmptyPageV2: PageV2<String> = PageV2<String>(items: [], hasMore: false)
public let EmptyPageV3: PageV3<String> = PageV3<String>(results: [], page: 0)

// The default handler for commen HTTP methods
public struct DefaultHandler {
   
    public static let applicationJson = "application/json"
    
    // the list result to return for any GET request
    public var listResult: Encodable?
    // true - will validate "Content-type" header for "application/json" for POST, PUT, PATCH requests
    public var validateJsonHeader: Bool = false
    
    // Default GET handler
    //
    // routes.add(method: .get, uri: "/**", handler: DefaultHandler.get())
    //
    // Test:
    // $ curl -v -X GET http://localhost:8080/no
    public func get() -> RequestHandler {
        return { request, response in
            response.status = HTTPResponseStatus.ok
            response.setHeader(.contentType, value: DefaultHandler.applicationJson)
            if let listResult = self.listResult {
                do {
                    try response.setBody(json: listResult.dictionary())
                }
                catch let error { print("ERROR: \(error)")}
            }
            else {
                response.setBody(string: "[]") // sample list response
            }
            response.next()
        }
    }
    
    // Default POST handler
    // Can be added for URI: /**
    // routes.add(method: .post, uri: "/**", handler: DefaultHandler.post())
    //
    // Test:
    // FAIL(header): $ curl -v -X POST http://localhost:8080/no -d '{}'
    // FAILT(body)   $ curl -v -X POST -H "Content-type: application/json" http://localhost:8080/no -d '{BAD REQUEST}'
    // SUCCESS:      $ curl -v -X POST -H "Content-type: application/json" http://localhost:8080/no -d '{}'
    public func post() -> RequestHandler {
        return { request, response in
            guard !self.validateJsonHeader || DefaultHandler.contentTypeJson(request, response) else { return }
            response.setHeader(.contentType, value: DefaultHandler.applicationJson)
            guard let body = request.postBodyString, var json = request.json(), !body.isEmpty else {
                response.status = HTTPResponseStatus.badRequest
                response.setError(DefaultError.wrongBody)
                response.next()
                return
            }
            
            json["id"] = JSON(Int.rand())
            response.status = HTTPResponseStatus.created
            _ = try? response.setBody(json: json)
            response.next()
        }
    }
    
    // Default DELETE handler
    //
    // routes.add(method: .delete, uri: "/**", handler: DefaultHandler.delete)
    //
    // $ curl -v -X DELETE http://localhost:8080/no
    public static func delete(request: HTTPRequest, response: HTTPResponse) {
        response.status = HTTPResponseStatus.ok
        if let body = request.postBodyString {
            response.setBody(string: body)
        }
        response.next()
    }
    
    // Default PUT handler
    //
    // routes.add(method: .put, uri: "/**", handler: DefaultHandler.put)
    //
    // FAIL(header): $ curl -v -X PUT http://localhost:8080/no -d '{"some":"some"}'
    // FAIL(body):   $ curl -v -X PUT -H "Content-type: application/json" http://localhost:8080/no
    // SUCCESS:      $ curl -v -X PUT -H "Content-type: application/json" http://localhost:8080/no -d '{"some":"some"}'
    public func put(request: HTTPRequest, response: HTTPResponse) {
        guard !validateJsonHeader || DefaultHandler.contentTypeJson(request, response) else { return }
        response.setHeader(.contentType, value: DefaultHandler.applicationJson)
        guard let body = request.postBodyString, let json = request.json(), !body.isEmpty else {
            response.status = HTTPResponseStatus.badRequest
            response.setError(DefaultError.wrongBody)
            response.next()
            return
        }
        response.status = HTTPResponseStatus.ok
        response.setBody(string: body)
        response.next()
    }
    
    // Default PATCH handler
    //
    // routes.add(method: .patch, uri: "/**", handler: DefaultHandler.patch)
    //
    // FAIL(body): $ curl -v -X PATCH -H "Content-type: application/json" http://localhost:8080/no
    // FAIL(type): $ curl -v -X PATCH http://localhost:8080/no -d '{"some":"some"}'
    // SUCCESS:    $ curl -v -X PATCH -H "Content-type: application/json" http://localhost:8080/no -d '{"some":"some"}'
    public func patch(request: HTTPRequest, response: HTTPResponse) {
        return put(request: request, response: response)
    }
    
    // MARK: - Helpful methods
    
    // Verifies .contentType header for "application/json".
    // If it's not JSON, then updates response with 400 and body so that client can just `return`, e.g.
    // `guard DefaultHandler.contentTypeJson(request, response) else { return }`
    public static func contentTypeJson(_ request: HTTPRequest, _ response: HTTPResponse) -> Bool {
        guard let header = request.header(.contentType), header == applicationJson else {
            response.status = .badRequest
            response.setError(type: "ERROR_Content-Type", description: "Wrong Content-Type header")
            response.next()
            return false
        }
        return true
    }
}

public extension HTTPResponse {
    
    // Set error body
    public func setError(type: String = "ERROR", description: String) {
        let response = self
        do {
            try response.setBody(json: DefaultError(type: type, description: description).dictionary())
        }
        catch let error { print("ERROR: \(error)") }
    }
    
    // Set error body
    public func setError(_ error: DefaultError) {
        do {
            try self.setBody(json: error.dictionary())
        }
        catch let error { print("ERROR: \(error)") }
    }
}
