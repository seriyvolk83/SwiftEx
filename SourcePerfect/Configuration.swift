//
//  Configuration.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 10/12/19.
//

import Foundation

/// A helper class to get the configuration data in the plist file.
open class Configuration: NSObject {
    
    // singleton
    public static let shared = Configuration()
    
    // port
    public static var port: Int {
        return 8080
    }
    
    // database name
    public static var databaseName: String {
        return ProcessInfo.processInfo.environment["DATABASE_NAME"] ?? "test"
    }
    
    // database host
    public static var databaseHost: String {
        #if DEBUG
            return "127.0.0.1"
        #else
            return ProcessInfo.processInfo.environment["DATABASE_HOST"] ?? "database"
        #endif
    }
    
    public static var databasePort: Int {
        #if DEBUG
        return 33060
        #else
        return Int(ProcessInfo.processInfo.environment["DATABASE_PORT"] ?? "") ?? 3306
        #endif
    }
    
    // database user
    public static var databaseUser: String {
        #if DEBUG
        return "root"
        #else
        return ProcessInfo.processInfo.environment["DATABASE_USERNAME"] ?? "root"
        #endif
    }
    
    // database password
    public static var databasePassword: String {
        #if DEBUG
        return "myPassword"
        #else
        return ProcessInfo.processInfo.environment["DATABASE_PASSWORD"] ?? "myPassword"
        #endif
    }

}
