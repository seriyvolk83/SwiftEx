//
//  CrudService.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 31.10.2019.
//

import Foundation
import PerfectHTTP
import PerfectCRUD
import PerfectMySQL

public typealias IdType = Int

// Defines any model object for using with CrudController and CrudService
public protocol CrudObject: Codable {
    var id: IdType! { get set }
}
// alias just to separate the CrudObject from DbObject in controllers
public typealias DbObject = CrudObject

// Provides methods for object convertion: (API:DbConvertable) -> DB and (DB:DbConvertable) -> API
public protocol DbConvertable {
    
    // Convert from current to other object. Usially API->DB, but you can also do DB->API
    func toDb() throws -> Any
}

// CRUD service for any object that implements CrudObject
/*
 1. Create model (structure) with CrudObject implementation.
 2. `public var service = CrudServiceMySQL<Model>()`
 3. Use methods from `CrudService`.
 */
public protocol CrudService {
    associatedtype Item
    
    /// Create object
    func create(_ object: Item) throws -> Item
    
    /// Update object
    func update(_ object: Item) throws
    
    /// Delete object
    func delete(_ object: Item) throws
    
    /// Delete by ID
    func delete(id: IdType) throws
    
    /// Select all objects
    func selectAll() throws -> [Item]
    
    /// Select object by ID
    func select(byId id: IdType) throws -> Item
}

// MySQL service
// Example of order:
// ```
// var order: PerfectCRUD.Ordering<Model, Table<Model, Database<MySQLDatabaseConfiguration>>> =
/// (sortOrder == "asc" ? t.order(by: \.createdOn) : t.order(descending: \.createdOn))
/// ```
public class CrudServiceMySQL<T: DbObject>: CrudService {
    
    public typealias Item = T
    
    public init(){}
    
    // the related table in MySQL
    public var table: Table<T, Database<MySQLDatabaseConfiguration>> {
        return DatabaseUtil.shared.database.table(T.self)
    }
    
    /// Create object in database (INSERT)
    // Updates `id` field after insertion
    public func create(_ object: T) throws -> T {
        var o = object
        //        o.id = Int.rand().toUUID()
        let id = try table.insert([o]).lastInsertId()
        o.id = IdType(id ?? 0)
        return o
    }
    
    /// Update object in database
    public func update(_ object: T) throws {
        try table.where(\T.id == object.id)
            .update(object)
    }
    
    /// Delete object in database
    public func delete(_ object: T) throws {
        guard let id = object.id else { throw "ID cannot be nil" }
        try delete(id: id)
    }
    
    /// Delete by ID
    public func delete(id: IdType) throws {
        let query = table.where(\T.id == id)
        try query.delete()
    }
    
    /// Select all objects
    public func selectAll() throws -> [T]  {
        let query = try table.select()
        var items = [T]()
        for item in query {
            items.append(item)
        }
        return items
    }
    
    /// Select object by ID
    public func select(byId id: IdType) throws -> T  {
        let query = try table.where(\T.id == id).select()
        var items = [T]()
        for item in query {
            items.append(item)
        }
        guard let item = items.first else { throw HTTPResponseError(status: .notFound, description: "Object with such ID is not found") }
        return item
    }
}
