//
//  CrudController.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 31.10.2019.
//

import Foundation
import PerfectHTTP
import PerfectLib
import SwiftExApi

// Controller for SIMPLE objects that have no other references
// Objects can optionally implement protocols: PostValidate, PutValidate
// ```
// routes.addCRUD(uri: "/user", handler: CrudController<UserInfo>().handler)
// ```
open class CrudController<T: CrudObject> {
    
    // database service
    public var service = CrudServiceMySQL<T>()
    
    public init() {
    }
    
    /// CRUD handler for Model object
    public func handler(request: HTTPRequest, response: HTTPResponse) throws {
        switch request.method {
            
        // GET handler
        case .get:
            let items = try service.selectAll()
            // dodo wrap into page
            // dodo pagination
            try response.response(items)
            
        // POST handler
        case .post:
            let object: T = try request.body()
            try validatePost(object)
            
            let newObject = try service.create(object)
            try response.response(newObject)
            response.status = .created
            
        // PUT handler
        case .put:
            let object: T = try request.body()
            try validatePut(object)
            
            /// Reset ID to the ID provided in the URL
            let id = try request.getId()
            var o = object
            o.id = id
            _ = try service.select(byId: id)
            
            try service.update(o)
            try response.response(o)
            
        // dodo PATCH handler
            
        // DELETE handler
        case .delete:
            let id = try request.getId()
            
            _ = try service.select(byId: id)
            
            try service.delete(id: id)
            
        // Other methods handler
        default:
            throw HTTPResponseError(status: .methodNotAllowed, description: "Method is not supported")
        }
        response.completed()
    }
    
    // Validate object for POST request.
    // Class should implement `PostValidate` method and throws the name of the invalid field(s)
    public func validatePost(_ object: Codable) throws {
        if let o = object as? PostValidate {
            do {
                try o.validatePost()
            } catch let field {
                throw HTTPResponseError(status: .badRequest, description: "Field '\(field)' is incorrect or missing")
            }
        }
    }
    
    // Validate object for PUT request.
    // Class should implement `PutValidate` method and throws the name of the invalid field(s)
    public func validatePut(_ object: Codable) throws {
        if let o = object as? PutValidate {
            do {
                try o.validatePut()
            } catch let field {
                throw HTTPResponseError(status: .badRequest, description: "Field '\(field)' is incorrect or missing")
            }
        }
    }
}

// Optional: provides method for any value validation for POST method
public protocol PostValidate {
    
    // validate POST requests for this object. Should throw field name as error if is.
    func validatePost() throws
}
// Optional: PUT validation
public protocol PutValidate {
    
    // validate PUT requests for this object. Should throw field name as error if is.
    func validatePut() throws
}
// Optional: PATCH validation
public protocol PatchValidate {
    
    // validate PATCH requests for this object. Should throw field name as error if is.
    func validatePatch() throws
}
