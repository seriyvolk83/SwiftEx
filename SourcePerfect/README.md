
# SwiftExPerfect Examples

## File download

### To local file, then reading local file
```
let g = DispatchGroup()
g.enter()
let url = URL(string: "http://<host>/tmp/swift/training.csv")!
DownloadUtil().download(url: url) { (location) in
    print("Downloaded: \(location)")

    do {
        let input = try InputStreamReader(url: location)
        input.kLimit = 10
        try input
            .log(every: 10000)
            .process { (str, k) in
                print(str)
        }
    }
    catch let error {
        print("ERROR: \(error)")
    }
    g.leave()
}
g.wait()
```

### To string (for small files)
```
let g = DispatchGroup()
g.enter()

DownloadUtil(baseUrl: "http://<host>/tmp/swift").process(endpoint: "/training.csv") { string in
    print(string)
    g.leave()
}
g.wait()
```

## Database access

### Simple model/table without any other relations

See example in `CompanySampleService.sample()`:

- Model is `CompanySample`;
- Service that stores/read data to/from MySQL is `CrudServiceMySQL<CompanySample>` (see `var service` in `CompanySampleService`);
- To create table initially that matches the model:
```
_ = try? DatabaseUtil.shared.database.create(CompanySample.self, primaryKey: \.id, policy: .reconcileTable)
```
Note: it will not add `auto_increment` to the table definition. You either should generate IDs manually (see `CompanySampleService.create` method) or you should launch once, then check `mysql` for `show create table CompanySample`, modify it manually or put `create table...` instruction into db initial scripts. Then remove table, run mysql again so that it will use the scripts to create a table, then relaunch the server.
- `DatabaseUtil` provide an example on how to create database connection. It uses ENV variables defined in `Configuration`.

### Two related tables - "parent - children"



