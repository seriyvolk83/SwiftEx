//
//  LogFilter.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 30.10.2019.
//

import Foundation
import PerfectHTTP
import SwiftExApi
import SwiftEx
//import PerfectCURL
import SwiftyJSON

/// Request filter that logs all requests with their parameters
/**
 do {
     try HTTPServer.launch(HTTPServer.Server.server(name: "localhost", port: 8080,
         routes: routes,
         requestFilters: [
            (LogFilter(loggerUrl: "http://localhost:8585"), .high)
         ],
         responseFilters: []))
 }
 catch {
    fatalError("\(error)")
 }
 */
public class LogFilter: HTTPRequestFilter {
    
    // API
    private let api: RESTApi
    
    /// Initializer
    ///
    /// - Parameter loggerUrl: the logger URL, e.g. "http://localhost:8585"
    public init(loggerUrl: String) {
        api = RESTApi(baseUrl: loggerUrl)
    }
    
    public func filter(request: HTTPRequest, response: HTTPResponse, callback: (HTTPRequestFilterResult) -> ()) {
        sendLogRequest(request)
//        var r = URLRequest(url: URL(string: "http://<server>/tmp/swift/logger.php")!)
//        r.httpMethod = "POST"
//        r.httpBody = log.data()
//        let session = URLSession.shared
//        let task = session.dataTask(with: r) { (data, response, error) in
//            print("DATA: \(data)")
//            print("ERROR: \(error)")
//        }
//        task.resume()
//        do {
//            let curl = CURLRequest("http://localhost:8585/", .postString(log))
//            let response = try curl.perform().bodyString
//            print("response: \(response)")
//        }
//        catch let error {
//            print("ERROR: \(error)")
//        }
        callback(.continue(request, response))
    }
    
    public func sendLogRequest(_ request: HTTPRequest) {
        let log = LogFilter.logRequest(request)
        api.sendLog(log: log)
    }
    
    public static func logRequest(_ request: HTTPRequest) -> String {
        // Log request URL
        var info = "url"
        info = request.method.description
        var logMessage = "\(Date())"
        logMessage += "[REQUEST]\t curl -v -X \(info) \"http://localhost:8080\(request.uri)\""
        
        // log body if set
        if let bodyAsString = request.postBodyString {
            logMessage += "\\\n\t -d '\(bodyAsString.replace("\n", withString: "\n"))'"
        }
        for h in request.headers {
            // Skip a few headers
            if h.0.standardName == "Connection" && h.1 == "keep-alive" { continue }
            if h.0.standardName == "Content-Length" { continue }
            logMessage += "\\\n\t -H \"\(h.0.standardName): \(h.1.replace("\"", withString: "\\\""))\""
        }
        print(logMessage)
        return logMessage
    }
}

extension RESTApi {
    
    public func sendLog(log: String, callback: (() -> ())? = nil, failure: FailureCallback? = nil) {
        let parameters: [String: Any] = [
            "log": log
        ]
        post("/", parameters: parameters, success: { _ in
            callback?()
        }, failure: failure ?? { _ in })
    }
}
