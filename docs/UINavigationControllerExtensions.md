# Documentation


Define in your project:

```swift

/**
* Extension adds methods that change navigation bar
*
* @author Volkov Alexander
* @version 1.0
*/
extension UIViewController {

    /// Changes navigation bar design to transparent
    func setTransparentNavigationBar() {
        setupNavigationBar(isTransparent: true, buttonColor: <#color#>)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: <#color#>, .font: <#font#>]
    }

    /// Set non-trasparent navigation bar with given color
    func setColoredNavigationBar() {
        setupNavigationBar(isTransparent: false, buttonColor: <#color#>, bgColor: <#color#>)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: <#color#>, .font: <#font#>]
    }
}

```

Usage in any view controller:
```swift

/// Setup navigation bar
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setTransparentNavigationBar()
}

/// Setup navigation bar
override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    setTransparentNavigationBar()
}
```

Colored navigation bar can be defined in AppDelegate:
```swift
// Navigation bar style
UINavigationBar.appearance().tintColor = UIColor.white
UINavigationBar.appearance().barTintColor = <#color#>
UINavigationBar.appearance().isTranslucent = false
UINavigationBar.appearance().shadowImage = UIImage(named: "shadow")
UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: <#color#>, .font: <#font#>]
```
