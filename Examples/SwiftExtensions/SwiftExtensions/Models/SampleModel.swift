//
//  SampleModel.swift
//  SwiftExtensions
//
//  Created by Volkov Alexander on 9/29/20.
//

import Foundation

/// Sample model object
struct SampleModel: Codable {
    
    let text: String
    let number: Int
    let anotherObject: SampleModelObject
}

struct SampleModelObject: Codable {
    let title: String
}
