//
//  User.swift
//  
//
//  Created by Volkov Alexander on 11/10/20.
//

import Vapor

struct User: Authenticatable, Content {
    
    var name: String!
    var date: Date
    
    // fields for verification example
    var username: String?
    var age: Int? // >=13
    var weight: Float?
    var label: String?
    var country: Country?
    
    enum Color: String, Content {
        case red, green
    }
    
    var color: Color?
    
    /// Runs after this Content is decoded.
    /// Used in `Request`.
    /// `$ curl -X GET -v 'http://localhost:8080/hello?name=' `
    mutating func afterDecode() throws {
        // Name may not be passed in, but if it is, then it can't be an empty string.
        self.name = self.name?.trimmingCharacters(in: .whitespacesAndNewlines)
        if let name = self.name, name.isEmpty {
            throw Abort(.badRequest, reason: "Name must not be empty.")
        }
    }
    
    /// Runs before this Content is encoded.
    /// Used for `Response`
    mutating func beforeEncode() throws {
        // Have to *always* pass a name back, and it can't be an empty string.
        guard
            let name = self.name?.trimmingCharacters(in: .whitespacesAndNewlines),
            !name.isEmpty
            else {
                throw Abort(.badRequest, reason: "Name must not be empty.")
        }
        self.name = name
    }
}

struct Country: Content {
    
    let id: String
    let title: String!
}

// MARK: - BasicAuthenticator

struct UserAuthenticator: BasicAuthenticator {
    typealias User = App.User
    
    func authenticate(basic: BasicAuthorization, for request: Request) -> EventLoopFuture<Void> {
        if basic.username == "topcoder" && basic.password == "rocks"
            || basic.username == "test" && basic.password == "secret" {
            request.auth.login(User(name: "topcoder", date: Date()))
        }
        print("BasicAuthorization: \(basic.username):\(basic.password)")
        return request.eventLoop.makeSucceededFuture(())
    }
}

// MARK: - Validatable

extension User: Validatable {
    
    /**
     Examples:
     ```
     try User.validate(query: r)
     
     try User.validate(content: r)
     ```
     */
    static func validations(_ validations: inout Validations) {
        
        // not empty
        validations.add("name", as: String.self, is: !.empty)
        
        // |username| >= 3 and in [a-ZA-Z0-9]
        validations.add("username", as: String.self, is: .count(3...) && .alphanumeric, required: false)
        
        // required date
        validations.add("date", as: Date.self, is: .valid, required: true)
        // TODO make own date validations (in range)
        
        // in range (Int)
        validations.add("age", as: Int.self, is: .range(13...), required: false)
        
        // in range (Float)
        validations.add("weight", as: Float.self, is: .range(0...), required: false)
        
        // nil or non-empty (PUT like)
        validations.add("label", as: String?.self, is: .nil || !.empty, required: false)
        
        // enum
        validations.add("color", as: String.self, is: .in("red", "green"), required: false)
        validations.add("country", required: false) { (countryValidations) in
            countryValidations.add("id", as: String.self, is: !.empty, required: true)
            countryValidations.add("title", as: String?.self, is: .nil || !.empty, required: false)
        }
    }
    
}
