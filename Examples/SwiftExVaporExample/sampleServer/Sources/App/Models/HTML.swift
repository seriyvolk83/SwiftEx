//
//  HTML.swift
//  
//
//  Created by Volkov Alexander on 11/10/20.
//

import Vapor

/// HTML content
struct HTML: Content {
    
    /// the html
    let value: String
}

extension HTML: ResponseEncodable {

    /// Encodes HTML as Content with correct `.contentType`.
    /// - Parameter request: the request
    /// - Returns: future
    public func encodeResponse(for request: Request) -> EventLoopFuture<Response> {
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: "text/html")
        return request.eventLoop.makeSucceededFuture(.init(
            status: .ok, headers: headers, body: .init(string: value)
            ))
    }
}
