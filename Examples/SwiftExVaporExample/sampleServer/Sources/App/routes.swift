import Vapor
import SwiftExVapor
import SwiftyJSON

func routes(_ app: Application) throws {
    
    /// Examples of GET/POST requests
    ExamplesController().configure(app: app)
    
    /// Customized general handler for `/users` endpoint. Will cache as `user`
    UsersController().configure(app: app)
    
    /// Customized general handler for CRUD
    GeneralController.shared.configure(app: app)
}
