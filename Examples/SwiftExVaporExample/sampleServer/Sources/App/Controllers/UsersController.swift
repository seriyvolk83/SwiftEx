//
//  UsersController.swift
//  
//
//  Created by Volkov Alexander on 11/10/20.
//

import Vapor
import SwiftExVapor
import SwiftyJSON

/// Customized general handler for `/users` endpoint. Will cache as `user`
public class UsersController {
    
    /// Add `/users` routes to Application
    public func configure(app: Application) {
        let userController = GeneralController()
        userController.path = "users"
        userController.idFieldName = "user_id"
        userController.idGenerate = {
            return "ID-" + (GeneralController.IdType.intAsString.generate() as! String)
        }
        userController.filterCallback = { (r, list) throws -> [JSON] in
            let page = try r.query.validInt("page") ?? 0
            let limit = try r.query.validInt("limit") ?? 10
            return list.filterByOffset(list, offset: page * limit, limit: limit).0
        }
        let initialPageFormatCallback = userController.pageFormatCallback
        userController.pageFormatCallback = { (r: Request, items: [JSON], total: Int) -> String in
            let page = Int(r.query["page"] ?? "") ?? 1
            let itemsString = initialPageFormatCallback(r, items, total)
            return """
            {
            "page": \(page),
            "items": \(itemsString),
            "total": \(total)
            }
            """
        }
        userController.configure(app: app)
    }
}
