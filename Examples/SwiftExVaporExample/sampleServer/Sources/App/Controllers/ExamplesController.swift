//
//  ExamplesController.swift
//  
//
//  Created by Volkov Alexander on 11/10/20.
//

import Vapor
import SwiftExVapor

/**
GET /auth
POST /test
GET hello/name
GET /validate
POST /validate
 
POST /remote
GET /remote/info
 
GET /delay
GET /sleep
 
GET /download
GET /html
 */
public class ExamplesController {
    
    public func configure(app: Application) {
        /// Basic requests to test the server
        app.get { req in
            return "It works!"
        }
        
        app.get("hello") { req -> String in
            return "Hello, world!"
        }
        
        /// Authorization
        let protected = app.grouped(UserAuthenticator())
            .grouped(User.guardMiddleware(
                // Correct version of 401 response
                throwing: Abort(.unauthorized, headers: ["WWW-Authenticate": "Basic"], reason: "User not authenticated.")
            ))
        let indexHtml = HTML(value: """
  <html>
    <body>
      <h1>Authenticated</h1>
    </body>
  </html>
  """)
        protected.get("auth") { req in
            return indexHtml
        }
        
        /// POST with decoding content
        /// `$ curl -X POST -H "Content-Type:application/json" -d '{"name":"some"}' -v http://localhost:8080/test `
        app.post("test") { req -> User in
            let user = try req.content.decode(User.self)  /// Conforming the type to `Content` will allow decoding
            let bodyString = "\(req.body.string ?? "")"   /// Can get BODY as a `String`
            print("POST: \(req); body=\"\(bodyString)\";")
            //        return HTTPStatus.created
            //        return "\(bodyString)"
            return user
        }
        
        /// GET with `path` parameter
        /// `$ curl -X GET http://localhost:8080/hello/Alex/ `
        app.get("hello", ":name") { (r) -> String in
            let name: String = r.parameters.get("name")!
            return "Hello, \(name)"
        }
        
        // MARK: - Validation
        
        /// GET with `query` parameter
        /// FAIL `$ curl -X GET 'http://localhost:8080/validate' `
        /// OK `$ curl -X GET 'http://localhost:8080/validate?name=Alex&date=2020-01-01&status=Good' `
        app.get("validate") { r -> User in
            //        let status: String? = req.query["status"]
            //        let name = user.name?.replace("Alex", withString: "Alexander") ?? "Anonymous"
            //        let id = Int.rand()
            //        return "Hello, \(name) [id generated=\(id), date=\(user.date)]" + (status != nil ? " [status=\(status!)]" : "")
            
            try User.validate(query: r)
            
            let user = try r.query.decode(User.self)
            return user
        }
        
        /// FAIL `$ curl -X POST 'http://localhost:8080/validate' `
        /// FAIL `$ curl -X POST -d '{"name":"Alex", "date":"2020-01-01"}' 'http://localhost:8080/validate' `     // no Content-Type
        /// OK `$ curl -X POST -H "Content-Type:application/json" -d '{"name":"Alex", "date":"2020-01-01"}' 'http://localhost:8080/validate' `
        /// OK `$ curl -X POST -H "Content-Type:application/json" -d '{"name":"Alex", "date":"2020-01-01", "country": {"id":"1", "title":"USA"}}' 'http://localhost:8080/validate' `
        app.post("validate") { r -> User in
            
            try User.validate(content: r)
            
            let user = try r.content.decode(User.self)
            return user
        }
        
        // MARK: - Async
        
        // Remote API IP
        var remoteApiUrl: String = (Environment.process.REMOTE_API_URL ?? "http://google.com") // http://localhost:8080
        if remoteApiUrl.hasSuffix("/") {
            remoteApiUrl = remoteApiUrl.clearEndingPath
        }
        
        // Sends request to another server (`REMOTE_API_URL`) and parses the response
        /// curl as above "POST /validate"
        app.post("remote") { r -> EventLoopFuture<User> in
            try User.validate(content: r)
            let user = try r.content.decode(User.self)
            
            let url: URI = URI(string: remoteApiUrl + "/validate")
            let remoteRequest = r.client.post(url) { req in
                try req.content.encode(user)
            }
            // Download Completion block - can be used to do something not related to response
            remoteRequest.whenComplete { (result) in
                print("Request Complete:")
                switch result {
                case .success(let string):
                    print(string) // The actual String
                case .failure(let error):
                    print(error) // A Swift Error
                }
                print("--")
            }
            // Parse User
            return remoteRequest.flatMapThrowing({ (res) -> User in
                let user = try res.content.decode(User.self)
                return user
            })
        }
        
        ///`$ curl -X GET 'http://localhost:8080/remote/info' `
        app.get("remote", "info") { r -> String in
            return remoteApiUrl
        }
        
        /// (make) Promise example
        /// OK `$ curl -X GET 'http://localhost:8080/delay' `
        app.get("delay") { (r) -> EventLoopFuture<String> in
            let promise = r.eventLoop.makePromise(of: String.self)
            delayBg(1) {
                if true {
                    promise.succeed("delayed")
                }
                //            else { promise.fail("Error") }
            }
            return promise.futureResult
        }
        
        /// Blocking work example (sleep)
        /// OK `$ curl -X GET 'http://localhost:8080/sleep' `
        app.get("sleep") { (r) -> EventLoopFuture<String> in
            /// Dispatch some work to happen on a background thread
            return r.application.threadPool.runIfActive(eventLoop: r.eventLoop) {
                /// Puts the background thread to sleep
                /// This will not affect any of the event loops
                sleep(3)
                
                /// When the "blocking work" has completed,
                /// return the result.
                return "Hello world, after a nap!"
            }
        }
        
        /// Download remove text
        /// `$ curl -X GET 'http://localhost:8080/download' `
        app.get("download") { (r) -> EventLoopFuture<String> in // EventLoopFuture<User> in
            return r.client.get("http://httpbin.org/status/201").map { $0.description }
            
            //        return r.client.get("http://google.com").flatMap { (res: ClientResponse) -> EventLoopFuture<User> in
            //            do {
            //                let user = try res.content.decode(User.self)
            //                return r.eventLoop.makeSucceededFuture(user)
            //            }
            //            catch let error {
            //                return r.eventLoop.makeFailedFuture(error)
            //            }
            //        }
        }
        
        /// Custom response encoding (defined in model object)
        /// `$ curl -X GET 'http://localhost:8080/html' `
        app.get("html") { _ in
            HTML(value: """
  <html>
    <body>
      <h1>Hello, World!</h1>
    </body>
  </html>
  """)
        }
    }
}
