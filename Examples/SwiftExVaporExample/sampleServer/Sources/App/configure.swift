import Vapor
import SwiftExVapor

// configures your application
public func configure(_ app: Application) throws {
    app.logger.info("Configuring...")
    
    // Configure custom port.
    app.http.server.configuration.port = 8082
//    app.http.server.configuration.address = BindAddress.hostname("127.0.0.1", port: 8082)
    
    // uncomment to serve files from /Public folder
    app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))
    
    // Custom date formatter
    do {
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        // create a new JSON encoder that uses unix-timestamp dates
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(f)
        // override the global encoder used for the `.json` media type
        ContentConfiguration.global.use(encoder: encoder, for: .json)
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(f)
        ContentConfiguration.global.use(decoder: decoder, for: .json)
        
        // Date encoder for GET parameters
        ContentConfiguration.global.use(urlDecoder: URLEncodedFormDecoder.createDateDecoder(formatter: f))
    }
    /// ContentDecoder and ContentEncoder.
    /// URLQueryDecoder and URLQueryEncoder.
    /// Another approach involves implementing `ResponseEncodable` on your types. Consider this trivial HTML wrapper type:
    
    // register routes
    try routes(app)
    
    print(app.routes.all.map({"\($0)"}).joined(separator: "\n"))
}
