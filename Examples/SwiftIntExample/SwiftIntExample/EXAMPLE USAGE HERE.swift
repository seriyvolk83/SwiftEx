//
//  ViewController.swift
//  SwiftIntExample
//
//  Created by Volkov Alexander on 26.10.2019.
//  Copyright © 2019 Alexander Volkov. All rights reserved.
//

import UIKit
import SwiftEx83

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        validateRestServiceApi()
    }

    
    /**
      DON'T forget to add the following in your project's Info.plist for debugging.
     <key>NSAppTransportSecurity</key>
     <dict>
     <key>NSAllowsArbitraryLoads</key>
     <true/>
     </dict>
     
     */
    func validateRestServiceApi() {
        // Initialize 401 handler (once when app is launched. Should be in AppDelegate or in the root view controller)
        RestServiceApi.callback401 = {
            print("ERROR: User is not authorized")
        }
        
        // EXAMPLE: HTTP GET call
        RestServiceApi.get(url: "http://api.plos.org/search?q=title:DNA")
            .call(on: self, { [weak self] (json: SampleJson) in
                
                print("->>>>>>>>>numFound: \(String(describing: json.response?.numFound))")
                
                self?.showAlert("Success", "Check Xcode console log for the response")
            })
    }

}


class SampleJson: Codable {
    var response: SampleJsonResponse?
}
class SampleJsonResponse: Codable {
    var numFound: Int?
}
