//
//  ViewController.swift
//  SwiftUIExample
//
//  Created by Volkov Alexander on 26.10.2019.
//  Copyright © 2019 Alexander Volkov. All rights reserved.
//

import UIKit
import SwiftEx83

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // EXAMPLE: `ActivityIndicator` usage
        /// Showing ActivityIndicator
        let indicator = ActivityIndicator(parentView: self.view).start()
        
        // EXAMPLE: `showAlert` usage
        let time: TimeInterval = 30
        delay(0.3) {
            self.showAlert("", "This is an example of ActivityIndicator usage. It will disappear in \(time) seconds. Tap OK and check.")
        }
        
        /// Dismiss after `time` seconds
        // EXAMPLE: `delay` usage
        delay(time) {
            // Remove the indicator
            indicator.stop()
        }
    }
}

