//
//  GeneralController.swift
//  
//
//  Created by Volkov Alexander on 11/6/20.
//

import Foundation
import SwiftExApi
import SwiftyJSON
import Vapor

/**
 CRUD controller. Stores data in memory.
 - GET:id, PUT, PATCH check ID --> 404 or 200
 - GET / has sorting, filter and pagination options.
 
 Usage:
 ```
 let userController = GeneralController()
 userController.path = "users"
 userController.idFieldName = "user_id"
 userController.idGenerate = {
    return "ID-" + (GeneralController.IdType.intAsString.generate() as! String)
 }
 userController.filterCallback = { (r, list) throws -> [JSON] in
     let page = try r.query.validInt("page") ?? 0
     let limit = try r.query.validInt("limit") ?? 10
     return list.filterByOffset(list, offset: page * limit, limit: limit).0
}
 let initialPageFormatCallback = userController.pageFormatCallback
 userController.pageFormatCallback = { (r: Request, items: [JSON], total: Int) -> String in
     let page = Int(r.query["page"] ?? "") ?? 1
     let itemsString = initialPageFormatCallback(r, items, total)
     return """
     {
     "page": \(page),
     "items": \(itemsString),
     "total": \(total)
     }
     """
 }
 userController.configure(app: app)
 
 // Cover all other requests
 GeneralController.shared.configure(app: app)
 ```
 */
public class GeneralController {

    public typealias Endpoint = String
    public typealias ObjectString = JSON
    
    public static let shared = GeneralController()
    
    /// the path used for the controller
    public var path: PathComponent = "*"
    
    // MARK: - ID parameters. It can generate ID in POST request
    public var idNeedToGenerate = true // generate ID and set in PUT request
    public var idFieldName: String = "id" // this should be set to correct value
    public var idType: IdType = .intAsString
    public enum IdType {
        case string, int, uuid, intAsString
        
        public func generate() -> Any {
            switch self {
            case .string: return UUID().uuidString.replace("-", withString: "").lowercased()
            case .int: return Int.rand(1000000)
            case .intAsString: return Int.rand(1000000).description
            case .uuid: return UUID().uuidString
            }
        }
    }
    /// the callback used to generate ID. If not provided, then `IdType.generate()` is used
    public var idGenerate: (()->(Any))!
    
    /// Filter callback. By default returns all
    public var filterCallback: ((Request, [JSON]) throws -> [JSON]) = { (r, list) throws -> [JSON] in
        return list
    }
    /// Sorting callback
    public var sortCallback: (([JSON])->[JSON]) = { list -> [JSON] in
        return list
    }
    
    /** Format GET body: request and items list as String (e.g. "[]"). Called after `sortCallback`.
    Can be as follows:
    ```
     return """
     {
        "items": \(items),
        "total": "123",
        "page": 1
     }
     """
     */
    public var pageFormatCallback: ((Request, [JSON], Int) -> String) = { r, items, total -> String in
        return "[" + items.map({$0.rawString() ?? ""}).joined(separator: ",") + "]"
    }
    
    // MARK: - Log parameters
    public var logGeneralWarning = true
    public var logRequest = false
    public var logEndpoint = false
    public var logListItemsWhenRequest = false
    
    // MARK: -
    
    /// the objects send to post and grouped by endpoint
    public var inMemoryCache = [Endpoint: [ObjectString]]()
    
    public init() {}
    
    public func configure(app: Application) {
        app.group(path) { group in
            // GET /**
            group.get(use: self.getList)
            
            // POST /**
            /// `$ curl -X POST -H "Content-Type:application/json" -d '{"fieldString": "Hello","fieldInt": 5,"fieldFloat": 3.14159,"fieldBool": true,"fieldDate": "2020-10-14 11:30:00.000"}' -v http://localhost:8080/objects `
            group.post(use: self.post)
            
            group.group(":id") { single in
                // GET /**/:id
                single.get(use: self.getById)
                // PATCH /**/:id
                single.patch(use: self.patch)
                // PUT /**/:id
                single.put(use: self.put)
                // DELETE /**/:id
                single.delete(use: self.delete)
            }
        }
        
        app.group("**") { group in
            // GET /**
            group.get { _ -> [String] in
                return []
            }
            // POST /**
            group.post { req -> String in
                let body = "\(req.body)"
                return body
            }
        }
    }
    
    // MARK: - GET/POST/GET:id/PUT/PATCH
    
    /// GET list of objects
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func getList(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [GET]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpoint
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        let list = inMemoryCache[endpoint] ?? []
        if logListItemsWhenRequest { print("list requested: \(list)") }
        
        let items = try filterCallback(r, sortCallback(list))
        let body = pageFormatCallback(r, items, list.count)
        
        /// Response 200
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
        return r.eventLoop.makeSucceededFuture(.init(
            status: .ok, headers: headers, body: .init(string: body)
            ))
    }
    
    /// POST general object
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func post(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [POST]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpoint
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        
        // Add object to `inMemoryCache` and return it.
        let body = "\(r.body.string ?? "")"
        var json = JSON(parseJSON: body)
        guard json != JSON.null else { throw Abort(HTTPResponseStatus.badRequest) }
        
        // Generate ID
        if idNeedToGenerate {
            json[idFieldName] = JSON(generateNewId())
        }
        
        // dodo set ID (add parameters to GeneralController to generate ID as string or int
        var list = inMemoryCache[endpoint] ?? []
        list.append(json)
        inMemoryCache[endpoint] = list
        if logListItemsWhenRequest { print("list updated: \(list)") }
        
        /// Response 201
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
//        headers.add(name: .location, value: "dodo?") // dodo get URL (host and path) and add ID
        return r.eventLoop.makeSucceededFuture(.init(
            status: .created,
            headers: headers, body: .init(string: json.rawString() ?? "")
            ))
    }
    
    /// PUT general object
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func put(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [PUT:id]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpointWithoutLastPath
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        // Parse object
        let body = "\(r.body.string ?? "")"
        var json = JSON(parseJSON: body)
        guard json != JSON.null else { throw Abort(HTTPResponseStatus.badRequest) }
        
        // Check if object exists
        let id: String? = r.parameters.get("id")
        var list = inMemoryCache[endpoint] ?? []
        if logListItemsWhenRequest { print("list to search: \(list)") }
        let object = list.filter({$0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil}).last
        guard let obj = object else { throw Abort(.notFound) }
        
        if idNeedToGenerate {
            json[idFieldName] = obj[idFieldName]
        }
        
        // Update object (remove and add)
        list = list.filter({ !($0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil)})
        list.append(json)
        inMemoryCache[endpoint] = list
        
        /// Response 200
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
        return r.eventLoop.makeSucceededFuture(.init(
            status: .ok,
            headers: headers, body: .init(string: json.rawString() ?? "")
            ))
    }
    
    /// PATCH general object
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func patch(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [PATCH:id]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpointWithoutLastPath
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        // Parse object
        let body = "\(r.body.string ?? "")"
        let json = JSON(parseJSON: body)
        guard json != JSON.null else { throw Abort(HTTPResponseStatus.badRequest) }
        
        // Check if object exists
        let id: String? = r.parameters.get("id")
        var list = inMemoryCache[endpoint] ?? []
        if logListItemsWhenRequest { print("list to search: \(list)") }
        let object = list.filter({$0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil}).last
        guard var obj = object else { throw Abort(.notFound) }
        
        // Update fields from request
        for (k,v) in json.dictionaryValue {
            obj[k] = v
        }
        
        // Replace object (remove and add)
        list = list.filter({ !($0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil)})
        list.append(obj)
        inMemoryCache[endpoint] = list
        
        /// Response 200
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
        return r.eventLoop.makeSucceededFuture(.init(
            status: .ok,
            headers: headers, body: .init(string: obj.rawString() ?? "")
            ))
    }
    
    /// DELETE general object
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func delete(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [PATCH:id]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpointWithoutLastPath
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        // Check if object exists
        let id: String? = r.parameters.get("id")
        var list = inMemoryCache[endpoint] ?? []
        if logListItemsWhenRequest { print("list to search: \(list)") }
        let object = list.filter({$0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil}).last
        guard let _ = object else { throw Abort(.notFound) }
        
        // Replace object (remove and add)
        list = list.filter({ !($0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil)})
        inMemoryCache[endpoint] = list
        
        /// Response 200
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
        return r.eventLoop.makeSucceededFuture(.init(
            status: .ok,
            headers: headers, body: .init(string: "")
            ))
    }
    
    /// GET object by ID
    /// - Parameter r: the request
    /// - Throws: the exception
    /// - Returns: the future with response
    public func getById(request r: Request) throws -> EventLoopFuture<Response> {
        if logGeneralWarning { print("WARNING! It's GeneralController [GET:id]") }
        if logRequest { print("Request: \(r)") }
        let endpoint = r.clearEndpointWithoutLastPath
        
        if logEndpoint { print("Endpoint: \(endpoint)") }
        
        let id: String? = r.parameters.get("id")
        
        let list = inMemoryCache[endpoint] ?? []
        if logListItemsWhenRequest { print("list to search: \(list)") }
        let object = list.filter({$0[idFieldName].stringValue == id || $0[idFieldName].intValue == Int(id ?? "") && Int(id ?? "") != nil}).last
        
        /// Response 200 or 404
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: ContentType.JSON.rawValue)
        if let object = object {
            return r.eventLoop.makeSucceededFuture(.init(
                status: .ok, headers: headers, body: .init(string: object.rawString() ?? "")
                ))
        }
        else {
            throw Abort(.notFound) // HTTP 404
        }
    }
    
    // MARK: -
    
    // Generates ID
    public func generateNewId() -> Any {
        if let idGenerate = idGenerate {
            return idGenerate()
        }
        return idType.generate()
    }
    
    // MARK: - Examples
    
    /** Example of method that can be used in the following code
    ```
     let controller = GeneralController()
     app.put("someobject", use: controller.put)
     ```
     */
    func putExample(request r: Request) throws -> EventLoopFuture<HTTPStatus> {
        return r.eventLoop.makeSucceededFuture(HTTPStatus.ok)
    }
    
    /// Example of configuring a route
    func configureExample(app: Application) {
        app.group("myexampleusers") { users in
            // GET /users
            users.get { _ -> [String] in
                return []
            }
            // POST /users
            users.post { req -> String in
                return "New User"
            }
            
            users.group(":id") { user in
                // GET /users/:id
                user.get { (req: Request) -> String in
                    let id = req.parameters.get("id")
                    return "User[id=\(id ?? "-")]"
                }
                // PATCH /users/:id
                user.patch { req in
                    return ""
                }
                // PUT /users/:id
                user.put { req in
                    return "Updated User"
                }
            }
        }
    }
}
