//
//  HelpfulFunctions.swift
//  
//
//  Created by Volkov Alexander on 11/6/20.
//

import Foundation
import Vapor
import SwiftExApi
import SwiftEx

extension String {
    
    /// Returns path without end "/", e.g. "/objects/" -> "objects"
    public var clearEndingPath: String {
        var str = self
        while str.hasSuffix("/") {
            str = str.substring(index: 0, length: str.length - 1)
        }
        while str.hasPrefix("/") {
            str = str.substring(index: 1, length: str.length)
        }
        return str
    }
}

extension Request {
    
    /// returns clear endpoint without ending "/" and "s", e.g. "/object/" -> "object", "/objects/" -> "object".
    /// It can be used as a key for the storage in POST, GET
    public var clearEndpoint: String {
        var str = self.url.path.clearEndingPath
        while str.hasSuffix("s") {
            str = str.substring(index: 0, length: str.length - 1)
        }
        return str
    }
    
    /// returns clear endpoint without ending "/" and "s" AND last path, e.g. "/object/123" -> "object", "objects/abc" -> "object".
    /// It can be used as a key for the storage in [GET:id] requests
    public var clearEndpointWithoutLastPath: String {
        let a = self.clearEndpoint.split(separator: "/")
        var str = a.count > 1 ? a[0..<(a.count-1)].map({String($0)}).joined(separator: "/") : self.clearEndpoint
        while str.hasSuffix("s") {
            str = str.substring(index: 0, length: str.length - 1)
        }
        return str
    }
}

extension URLQueryContainer {
    
    /// Return valid int parameter
    /// - Parameter key: the field name
    /// - Throws: if parameter is presented and is not Int
    public func validInt(_ key: String) throws -> Int? {
        let value: String? = self[key]
        guard value?.isNumber() ?? true else { throw Abort(HTTPResponseStatus.badRequest) }
        if let v = value {
            guard Int(v) != nil else { throw Abort(HTTPResponseStatus.badRequest) }
        }
        return Int(value ?? "")
    }
}

extension URLEncodedFormDecoder {
    
    /// Creates decoder with custom date format
    /// - Parameter dateFormat: the date format, e.g. "yyyy-MM-dd'T'HH:mm:ssZ" (iso)
    public static func createDateDecoder(dateFormat: String) -> URLQueryDecoder {
        let f = DateFormatter()
        f.dateFormat = dateFormat
        return createDateDecoder(formatter: f)
    }
    
    /** Creates decoder with custom date formatter
     - Parameter formatter: the formatter
     
     Example:
     ```
     let f = DateFormatter()
     f.dateFormat = "yyyy-MM-dd"
     
     // Date encoder for GET parameters
     ContentConfiguration.global.use(urlDecoder: URLEncodedFormDecoder.createDateDecoder(formatter: f))
     ```
     */
    public static func createDateDecoder(formatter: DateFormatter) -> URLQueryDecoder {
        let urlDecoder: URLQueryDecoder = URLEncodedFormDecoder(configuration: .init(dateDecodingStrategy: .custom({ (d: Decoder) -> Date in
            let string = try d.singleValueContainer().decode(String.self)
            if let date = formatter.date(from: string) {
                return date
            }
            throw Abort(HTTPResponseStatus.badRequest)
        })))
        return urlDecoder
    }
}

/// Delay execution
///
/// - Parameters:
///   - delay: the delay in seconds
///   - inQueue: the queue
///   - callback: the callback to invoke after 'delay' seconds
public func execute(after delay: TimeInterval, inQueue: DispatchQueue, _ callback: @escaping ()->()) {
    #if os(Linux)
    callback()
    #else
    let delay = delay * Double(NSEC_PER_SEC)
    let popTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC);
    inQueue.asyncAfter(deadline: popTime, execute: {
        callback()
    })
    #endif
}

/** shortcut for `execute(after:inQueue:callback)`
 Example usage:
 ```
 /// Promise example
 /// OK `$ curl -X GET 'http://localhost:8080/hello3' `
 app.get("hello3") { (r) -> EventLoopFuture<String> in
     let promise = r.eventLoop.makePromise(of: String.self)
     delayBg(3) {
         if true {
            promise.succeed("delayed")
         }
         else { promise.fail("Error") }
     }
     return promise.futureResult
 }
 ```
 */
public func delayBg(_ delay: TimeInterval, _ callback: @escaping ()->()) {
    execute(after: delay, inQueue: DispatchQueue.global(qos: .userInitiated), callback)
}

extension Int {
    
    public func positive() throws {
        if self <= 0 {
            throw Abort(.badRequest)
        }
    }
    
    public func nonNegative() throws {
        if self < 0 {
            throw Abort(.badRequest)
        }
    }
}

extension Response {
    
    /// Create response with given status and contetn
    /// - Parameters:
    ///   - status: HTTP status, e.g. `.created`
    ///   - body: the object for the body
    ///   - contentType: the content type header value
    public static func createResponse<T>(status: HTTPResponseStatus, body: T, contentType: String = ContentType.JSON.rawValue) throws -> Response where T: Codable {
        var headers = HTTPHeaders()
        headers.add(name: .contentType, value: contentType)
        
        let data = try JSONEncoder().encode(body)
        let string = String(data: data, encoding: .utf8) ?? ""
        
        return Response(status: .created, headers: headers, body: .init(string: string))
    }
}

/// Helpful methods for converting Encodable classes
extension Encodable {
    
    /// Convert json string
    public func asJsonString() throws -> String {
        let data = try JSONEncoder().encode(self)
        return String(data: data, encoding: .utf8) ?? ""
    }
    
    /// Convert object to another type using intermidiate JSON object
    ///
    /// - Returns: the decoded object
    public func convertMe<T>() -> T where T : Decodable {
        let data = try! JSONEncoder().encode(self)
        let result = try! JSONDecoder().decode(T.self, from: data)
        return result
    }
    
    /// Convert object to another type using intermidiate JSON object
    ///
    /// - Returns: the decoded object
    public func convertMeThrowing<T>() throws -> T where T : Decodable {
        let data = try JSONEncoder().encode(self)
        let result = try JSONDecoder().decode(T.self, from: data)
        return result
    }
}
