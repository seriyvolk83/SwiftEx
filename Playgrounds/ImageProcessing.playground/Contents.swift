//: A UIKit based Playground for presenting user interface
  
import UIKit
import QuartzCore   // for the basic animation
import XCPlayground // for the live preview
import PlaygroundSupport
import Accelerate

let imageView = UIImageView()
imageView.frame = CGRect(origin: .zero, size: CGSize(width: 626, height: 626))
imageView.contentMode = .topLeft

// start a live preview of that view
PlaygroundPage.current.liveView = imageView
let images = Array(1...5).map({UIImage(named: "sampleImage\($0).png")!})
print(images)

/// Image buffers
var buffers = images.map({ image -> vImage_Buffer in
    let cgImage = image.cgImage!
    let format = vImage_CGImageFormat(cgImage: cgImage)!
    let sourceBuffer = try! vImage_Buffer(cgImage: cgImage, format: format)
    return sourceBuffer
})
var formats = images.map({ image -> vImage_CGImageFormat in
    let cgImage = image.cgImage!
    let format = vImage_CGImageFormat(cgImage: cgImage)!
    return format
})
//#####################################################################

func rotateNinety(source: vImage_Buffer, format: vImage_CGImageFormat, rotation: Int) -> UIImage? {
//    guard var destinationBuffer = try? vImage_Buffer(width: Int(source.width),
//                                                     height: Int(source.height),
//                                                     bitsPerPixel: format.bitsPerPixel) else {
//        return nil
//    }
//    defer {
//        destinationBuffer.free()
//    }
    guard var destination: vImage_Buffer = {
        switch rotation {
        case kRotate0DegreesClockwise, kRotate180DegreesClockwise:
            return try? vImage_Buffer(size: source.size,
                                      bitsPerPixel: 8 * 4)
        case kRotate90DegreesClockwise, kRotate270DegreesClockwise:
            return try? vImage_Buffer(width: Int(source.size.height),
                                      height: Int(source.size.width),
                                      bitsPerPixel: 8 * 4)
        default:
            print("Unsupported rotation constant: \(rotation).")
            return nil
        }
    }() else {
        print("Unable to initialize destination buffer.")
        return nil
    }
    
    // 2. Apply the transform to `source` and write the result to `destination`.
    _ = withUnsafePointer(to: source) { sourcePtr in
        vImageRotate90_ARGB8888(sourcePtr,
                                &destination,
                                UInt8(rotation),
                                [0],
                                vImage_Flags(kvImageNoFlags))
    }
    
    let result = try? destination.createCGImage(format: format)
    if let result = result {
        return UIImage(cgImage: result)
    }
    else {
        return nil
    }
}

/// Convert buffer to image
func bufferToImage(source: vImage_Buffer, format: vImage_CGImageFormat) -> UIImage? {
    guard let result = try? source.createCGImage(format: format) else { return nil }
    return UIImage(cgImage: result)
}

func imageToBuffer(image: UIImage) -> vImage_Buffer {
    let cgImage = (image.cgImage ?? CIContext(options: nil).createCGImage(image.ciImage!, from: image.ciImage!.extent))!
    let format = vImage_CGImageFormat(cgImage: cgImage)!
    let sourceBuffer = try! vImage_Buffer(cgImage: cgImage, format: format)
    return sourceBuffer
}

func formatFrom(image: UIImage) -> vImage_CGImageFormat {
    let cgImage = (image.cgImage ?? CIContext(options: nil).createCGImage(image.ciImage!, from: image.ciImage!.extent))!
    let format = vImage_CGImageFormat(cgImage: cgImage)!
    return format
}

/// Creates buffer
func createBuffer(size: CGSize, format: vImage_CGImageFormat) -> vImage_Buffer? {
    return try? vImage_Buffer(width: Int(size.width),
                                                     height: Int(size.height),
                                                     bitsPerPixel: format.bitsPerPixel)
}

extension Pixel_8 {
    static let black: [Pixel_8] = [0,0,0,0]
    static let white: [Pixel_8] = [255,255,255,0]
}
/// Shifts image into a given buffer
func shift(image source: vImage_Buffer, to destination: inout vImage_Buffer, backgroundColor: [Pixel_8] = [0, 0, 0, 0], shift: CGPoint) {
    
    // 2. Create the affine transform that represents the scale-translate.
    var vImageTransform = vImage_CGAffineTransform(
        a: 1, b: 0,
        c: 0,     d: 1,
        tx: Double(shift.x),   ty: Double(shift.y))
//    tx: Double(0),   ty: Double(0))
    
    // 3. Apply the transform to `source` and write the result to `destination`.
    _ = withUnsafePointer(to: source) { srcPointer in
        vImageAffineWarpCG_ARGB8888(srcPointer,
                                    &destination,
                                    nil,
                                    &vImageTransform,
                                    backgroundColor,
                                    vImage_Flags(kvImageBackgroundColorFill))
    }
}

func blendImages(source1: vImage_Buffer, source2: vImage_Buffer, to destination: inout vImage_Buffer) {
    withUnsafePointer(to: source1) { srcPointer1 in
        _ = withUnsafePointer(to: source2) { srcPointer2 in
            vImagePremultipliedAlphaBlendLighten_RGBA8888(
//            vImagePremultipliedAlphaBlendDarken_RGBA8888(
//                vImagePremultipliedAlphaBlendScreen_RGBA8888(
                srcPointer1, srcPointer2, &destination, vImage_Flags(kvImageNoFlags))
        }
    }
}

func copyBuffer(source: vImage_Buffer) -> vImage_Buffer? {
    guard var destination = try? vImage_Buffer(width: Int(source.width),
                                               height: Int(source.height),
                                               bitsPerPixel: 32) else {
        return nil
    }
    let bytesPerPixel = 4
    withUnsafePointer(to: source) { src in
        vImageCopyBuffer(src,
                         &destination,
                         bytesPerPixel,
                         vImage_Flags(kvImageNoFlags))
    }
    return destination
}

func copyBuffer(from source: vImage_Buffer, to destination: inout vImage_Buffer) {
    let bytesPerPixel = 4
    withUnsafePointer(to: source) { src in
        vImageCopyBuffer(src,
                         &destination,
                         bytesPerPixel,
                         vImage_Flags(kvImageNoFlags))
    }
}

// WARNING! It does not work because adds yellow color to white when `blendImages`
/// Merge images into one
func mergeImages(images: [UIImage], shifts: [CGPoint], size: CGSize) -> UIImage? {
    /// Image buffers
    var buffers = images.map({ image -> vImage_Buffer in
        let cgImage = image.cgImage!
        let format = vImage_CGImageFormat(cgImage: cgImage)!
        let sourceBuffer = try! vImage_Buffer(cgImage: cgImage, format: format)
        return sourceBuffer
    })
    
    let formats = images.map({ image -> vImage_CGImageFormat in
        let cgImage = image.cgImage!
        let format = vImage_CGImageFormat(cgImage: cgImage)!
        return format
    })
//    for i in 0..<buffers.count {
//        buffers[i] = unpremultiplyData(source: buffers[i], format: formats[i])
//    }
    defer { for b in buffers { b.free() } }
    
    guard images.count == shifts.count, images.count > 0 else { return nil }
    guard let format = formats.first else { return nil }
    guard var destination = createBuffer(size: size, format: format) else { return nil }
    defer { destination.free() }
    
    for i in 0..<buffers.count {
        // Extended image destination
        guard var extendedImage = createBuffer(size: size, format: format) else { return nil }
        defer { extendedImage.free() }
        // Intermidiate result
        guard let resultImage = copyBuffer(source: destination) else { return nil}
        defer { resultImage }
           
        shift(image: buffers[i], to: &extendedImage, shift: shifts[i])
        if i == 0 {
            copyBuffer(from: extendedImage, to: &destination)
        }
        else {
            blendImages(source1: destination, source2: extendedImage, to: &destination)
        }
    }
//    destination = premultiplyData(source: destination, format: format)
    let image = bufferToImage(source: destination, format: format)
    
    return image
}

/// Merge images into one
func mergeImages2(images: [UIImage], shifts: [CGPoint], size: CGSize) -> UIImage? {
    /// Image buffers
    let buffers = images.map({ imageToBuffer(image: $0) })
    let formats = images.map({ formatFrom(image: $0) })
    defer { for b in buffers { b.free() } }
    
    guard images.count == shifts.count, images.count > 0 else { return nil }
    guard let format = formats.first else { return nil }
    guard var destination = createBuffer(size: size, format: format) else { return nil }
    defer { destination.free() }
    
    for i in 0..<buffers.count {
        copyImage(from: buffers[i], to: &destination, shift: shifts[i])
    }
    //    destination = premultiplyData(source: destination, format: format)
    let image = bufferToImage(source: destination, format: format)
    
    return image
}

func copyImage(from source: vImage_Buffer, to destination: inout vImage_Buffer, shift: CGPoint) {
    let destinationSize = CGSize(width: CGFloat(destination.width), height: CGFloat(destination.height))
    var roi: CGRect = CGRect(origin: shift, size: CGSize(width: CGFloat(source.width), height: CGFloat(source.height))) // optimize
    print("roi: \(roi)")
    let bytesPerPixel = 4
    
    if !(Int(roi.maxX) <= Int(destinationSize.width) && Int(roi.maxY) <= Int(destinationSize.height) &&
            Int(roi.minX) >= 0 && Int(roi.minY) >= 0) {
        var w: CGFloat = roi.width
        if max(shift.x, 0) + w > destinationSize.width {
            w = destinationSize.width - max(shift.x, 0)
        }
        var h: CGFloat = roi.height
        if max(shift.y, 0) + h > destinationSize.height {
            h = destinationSize.height - max(shift.y, 0)
        }
        roi = CGRect(x: max(shift.x, 0), y: max(shift.y, 0), width: w, height: h)
    }
    
    let start = Int(roi.origin.y) * destination.rowBytes + Int(roi.origin.x) * bytesPerPixel
    
    var destinationBuffer = vImage_Buffer(data: destination.data.advanced(by: start),
                                           height: vImagePixelCount(roi.height),
                                           width: vImagePixelCount(roi.width),
                                           rowBytes: destination.rowBytes)
    copyBuffer(from: source, to: &destinationBuffer)
}

func unpremultiplyData(source: vImage_Buffer, format: vImage_CGImageFormat) -> vImage_Buffer {
    guard var destinationBuffer = try? vImage_Buffer(width: Int(source.width),
                                                     height: Int(source.height),
                                                     bitsPerPixel: format.bitsPerPixel) else {
        return source
    }
    withUnsafePointer(to: source) { src in
        vImageUnpremultiplyData_ARGB8888(src, &destinationBuffer, vImage_Flags(kvImageNoFlags))
    }
    source.free()
    return destinationBuffer
}

func premultiplyData(source: vImage_Buffer, format: vImage_CGImageFormat) -> vImage_Buffer {
    guard var destinationBuffer = try? vImage_Buffer(width: Int(source.width),
                                                     height: Int(source.height),
                                                     bitsPerPixel: format.bitsPerPixel) else {
        return source
    }
    withUnsafePointer(to: source) { src in
        vImagePremultiplyData_ARGB8888(src, &destinationBuffer, vImage_Flags(kvImageNoFlags))
    }
    source.free()
    return destinationBuffer
}

defer {
    for b in buffers {
        b.free()
    }
}

//#####################################################################
//let result = images[0]
//let result = rotateNinety(source: buffers[0], format: formats[0], rotation: kRotate90DegreesClockwise)

var shifts = [CGPoint]()
for i in 0..<images.count {
    shifts.append(CGPoint(x: CGFloat(110 * i), y: 10))
}
let result = mergeImages2(images: images, shifts: shifts, size: CGSize(width: 500, height: 100))

//do {
//    let image1 = UIImage(named: "s1.png")!
//    let image2 = UIImage(named: "s2.png")!
//    let b1 = imageToBuffer(image: image1)
//    let b2 = imageToBuffer(image: image2)
//    let format1 = formatFrom(image: image1)
//    var b12 = try! vImage_Buffer(width: Int(b1.width),
//                                                    height: Int(b1.height),
//                                                    bitsPerPixel: format1.bitsPerPixel)
//
//    var b22 = try! vImage_Buffer(width: Int(b2.width),
//                                 height: Int(b2.height),
//                                 bitsPerPixel: format1.bitsPerPixel)
//
//    shift(image: b1, to: &b12, backgroundColor: Pixel_8.white, shift: CGPoint(x: 10, y: 0))
//    shift(image: b2, to: &b22, shift: CGPoint(x: 0, y: 0))
//    var destinationBlendBuffer = try! vImage_Buffer(width: Int(b1.width),
//                                                     height: Int(b1.height),
//                                                     bitsPerPixel: format1.bitsPerPixel)
//    blendImages(source1: b12, source2: b22, to: &destinationBlendBuffer)
//    //copyBuffer(from: b12, to: &destinationBlendBuffer)
//    let result = bufferToImage(source: destinationBlendBuffer, format: format1)
//}
imageView.image = result
imageView.backgroundColor = .darkGray
